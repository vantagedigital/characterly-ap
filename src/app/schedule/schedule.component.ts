import { GeneralList } from './../_models/general-code';
import { BranchService } from './../_services/branch.service';
import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ScheduleService } from '../_services/schedule.service';
import { forEach } from '@angular/router/src/utils/collection';
import { ScheduleEvent, Schedule, months } from '../_models/schedule-model';
import { CalendarComponent } from 'ng-fullcalendar';
import { Options } from 'fullcalendar';
import { ContentService } from '../_services/content.service';
import {ScheduleDialogComponent} from './schedule-dialog/schedule-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import "assets/js/bootstrap.min.js";
import { BsModalComponent } from 'ng2-bs3-modal';

import * as $ from 'jquery';


@Component({
  selector: 'app-schedule',
  // template: 'AppSchedule',
  templateUrl: './schedule.component.html',
  styleUrls:['./schedule.component.css'],
  entryComponents:[ScheduleDialogComponent],
  encapsulation: ViewEncapsulation.None
})

export class AppScheduleComponent implements OnInit {



  submitted=false;
  calendarOptions:Options = {
    //   height: 'parent',

    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,basicWeek,basicDay'
    },
    defaultDate: new Date(),
    navLinks: true,

    // fixedWeekCount : false,
    editable: false,
    eventLimit: true, // allow "more" link when too many events
    events: []
  };

  scheduleList: any[] = [];
  flag = false;
  dates = new Date;
  // calendarOptions: any;
  searchResult: GeneralList[] = [];
  selectedId: string;
  sword: string;
  displayList: boolean = false;
  contentDatasource: GeneralList[] = [];
  branchList: GeneralList[] = [];
  scheduleDate: string;
  loading = false;
  months: typeof months = months;

  isEdit = false;
//@ViewChild('myModal') el:ElementRef;
@ViewChild('calender', { read: ElementRef }) myCal: ElementRef;


@ViewChild('f') f:ElementRef;
@ViewChild('myModal')
//@ViewChild('calender') calender:ElementRef;

modal: BsModalComponent;
scheduleObj:any = {};//{StartDate: Date.now(), EndDate: Date.now()};


close() {
    this.modal.close();
}

open() {
    this.modal.open();
    this.isEdit = true;
}

createNewSchedule(){
  this.scheduleObj = {};
  this.scheduleObj.StartDate = new Date().toISOString().substring(0, 10);
  this.scheduleObj.EndDate = new Date().toISOString().substring(0, 10);

  this.open();
  this.isEdit = false;
}
  constructor(
    private schedule: ScheduleService,
    private contentService: ContentService,
    private branch: BranchService,
    public dialog: MatDialog,) {

  }

ngAfterViewInit(){

}
  ngOnInit() {
    this.calendarChange();
    this.getScheduleList();

    this.branch.GetBranchesList()
      .subscribe(response => {
        for (let index = 0; index < response.result.items.length; index++) {
          const element = response.result.items[index];
          let branchObj = new GeneralList;
          branchObj.id = element.id;
          branchObj.Name = element.name;

          this.branchList.push(branchObj);
        }
      })
    this.contentService.getContents(0, 10)
      .subscribe((data) => {

        // for (let index = 0; index < data.result.length; index++) {
        //   const element = data.result[index];
        //   let list = new GeneralList;
        //   list.id = element.id;
        //   list.Name = element.title;
        //   this.contentDatasource.push(list);
        // }
        console.log("Content List: " +data.result);
        for (let index = 0; index < data.result.length; index++) {
          const element = data.result[index];
          let list = new GeneralList;
          list.id = element.id;
          list.Name = element.title;
          this.contentDatasource.push(list);
        }
        this.searchResult = this.contentDatasource;
        //  this.dataService = this.completerService.local(this.contentDatasource , 'title', 'title');

      }, (err) => {
        console.log(err);
      });
  }
  getScheduleList(){
    this.scheduleList=[];
    this.schedule.GetScheduleList()
      .subscribe(response => {
        console.log(response);
        for (let index = 0; index < response.result.length; index++) {
          const element = response.result[index];
          if (element.startDate != null && element.endDate != null) {
            let sche:any = {};
            sche = element;
            sche.title= element.title;
            sche.start= element.startDate;
            sche.end= element.endDate;

            // let sche: object = {
            //   title: element.title,
            //   start: element.startDate,
            //   end: element.endDate
            // }
            this.scheduleList.push(sche);
          }

        }

        //this.caledarChange();
        //$(this.myCal.nativeElement).fullCalendar( 'removeEventSource', this.scheduleList );
      //  if($(this.myCal.nativeElement)){


        this.calendarOptions.events = this.scheduleList;
        //$(this.myCal.nativeElement).fullCalendar('renderEvents', this.scheduleList, true);
        $('#calender').fullCalendar('renderEvents', this.scheduleList, true);
        //$(this.myCal.nativeElement).fullCalendar('addEventSource', this.scheduleList);
      //  }

       //$(this.myCal.nativeElement).fullCalendar('render');
       //$(this.myCal.nativeElement).fullCalendar('removeEventSource', this.scheduleList);
        //$(this.myCal.nativeElement).fullCalendar( 'refetchEventSources', this.scheduleList );
        //this.scheduleList = response.result;



      }, (err) => {
        console.log(err);
      });
  }

  calendarChange() {
    var events = [ {
      title: 'All Day Event',
      start: '2016-09-01'
    },
    {
      title: 'Long Event',
      start: '2016-09-07',
      end: '2016-09-10'
    }];

    if (this.scheduleList.length > 0 && this.flag == false) {

      let date = new Date();
      var CurrntMonth: string[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",];

      var CrrntMonth = this.months[CurrntMonth[date.getMonth()]];
      let cDate = date.getUTCFullYear() + "-" + CrrntMonth + "-" + date.getUTCDate();
      // this.calendarOptions = {
      //   //   height: 'parent',

      //   header: {
      //     left: 'prev,next today',
      //     center: 'title',
      //     right: 'month,basicWeek,basicDay'
      //   },
      //   defaultDate: cDate,
      //   navLinks: true,

      //   // fixedWeekCount : false,
      //   editable: false,
      //   eventLimit: true, // allow "more" link when too many events
      //   events: events,
      //   eventClick: (event)=> {
      //         const data ={
      //           title:event.title,
      //           description:event.description
      //         };
      //          console.log(event);
      //          this.scheduleObj.Title=event.title;
      //          this.scheduleObj.Description=event.description;
      //          this.scheduleObj.BranchId=event.branchId;
      //          this.scheduleObj.ContentId = event.contentId;
      //          this.scheduleObj.Language= "Es-es";
      //          this.scheduleObj.CountryId = 0;
      //          this.scheduleObj.Status = 2;
      //          this.scheduleObj.ClassroomIds= [];
      //         //  this.scheduleObj.sword=event.sword
      //         //  this.scheduleObj.allDay=event.title
      //          this.scheduleObj.StartDate=event.start
      //          this.scheduleObj.EndDate=event.end
      //          this.scheduleObj.source =event.source

      //          this.open();
      //          //this.openDialog(data,"update");
      //         // this.el.nativeElement.value["title"] = 'This is title';
      //          //this.el.nativeElement.modal('show');
      //         return false;

      // }
      // };
      this.flag = true;
    }

    return true;
  }

  onDateChange(scheduleDate) {
    console.log("this is change");
    console.log(scheduleDate);

  }


  searchWord(word) {
    if (word === "") {
      this.sword = "";
      this.scheduleObj.ContentId = "";
      this.displayList = false;
    }
    else {
      this.displayList = true;
      console.log(word);
      this.searchResult = this.contentDatasource.filter(function (el: any) {
        return el.Name.toLowerCase().indexOf(word) > -1;
      });
    }

  }

  fillTextbox(contentList) {
    this.sword = "";
    this.scheduleObj.ContentId  = "";

    this.displayList = false;
    console.log(contentList);
    this.sword = contentList.Name;
    this.scheduleObj.ContentId = contentList.id;

  }

  createSchedule(f) {

    if(!f.valid){
      this.submitted=true;
      return;
    }
    
      this.loading = true;
      let data:any = {
        BranchId: this.scheduleObj.BranchId,
        ContentId:  this.scheduleObj.ContentId, //"5e121d9a-0fc0-4ca3-2490-08d535bb9c12",
        ClassroomIds: [],
        Language: "Es-es",
        CountryId: 79,
        Status: 2,
        Title: this.scheduleObj.Title,
        Description: this.scheduleObj.Description,
        StartDate: this.scheduleObj.StartDate,
        EndDate: this.scheduleObj.EndDate
      }

      if (this.isEdit ){
        data.id = this.scheduleObj.id;
        //console.log(data);
        this.schedule.updateSchedule(data)
          .subscribe(response => {
            this.loading = false;
            this.scheduleObj = {};
            this.close();
            this.getScheduleList();
          },
          (error)=>{
            this.loading = false;
            console.log(error);
          });
      } else {
        this.schedule.createSchedule(data)
          .subscribe(response => {
            this.loading = false;
            this.scheduleObj = {};
            this.close();
            this.getScheduleList();
          },
          (error)=>{
            this.loading = false;
            console.log(error);
          });
      }

    }
    eventClick(event) {
      const data ={
        title:event.title,
        description:event.description
      };
       this.scheduleObj.id = event.id;
       this.scheduleObj.Title=event.title;
       this.scheduleObj.Description=event.description;
       this.scheduleObj.BranchId=event.branchId;
       this.scheduleObj.ContentId = event.contentId;
       this.sword = this.contentDatasource.find( (el: any) =>   el.id ==event.contentId).Name;

       this.scheduleObj.Language= "Es-es";
       this.scheduleObj.CountryId = 0;
       this.scheduleObj.Status = 2;
       this.scheduleObj.ClassroomIds= [];
      //  this.scheduleObj.sword=event.sword
      //  this.scheduleObj.allDay=event.title

       this.scheduleObj.StartDate=new Date(event.start).toISOString().substring(0, 10);
       this.scheduleObj.EndDate= new Date(event.end).toISOString().substring(0, 10);
       this.scheduleObj.source =event.source

       this.open();
       //this.openDialog(data,"update");
      // this.el.nativeElement.value["title"] = 'This is title';
       //this.el.nativeElement.modal('show');
      return false;

      }
}

