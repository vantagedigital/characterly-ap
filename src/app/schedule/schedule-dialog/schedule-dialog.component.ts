import { Component, OnInit, Inject,EventEmitter,Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
@Component({
  selector: 'app-schedule-dialog',
  templateUrl: './schedule-dialog.component.html',
  styleUrls: ['./schedule-dialog.component.css']
})
export class ScheduleDialogComponent implements OnInit {
  @Output() formSubmit$: EventEmitter<boolean>;
  public form: FormGroup;
  
  constructor(
    private fb: FormBuilder,
		public updateDialogRef: MatDialogRef<ScheduleDialogComponent>,
		public dialog: MatDialog,
		@Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.formSubmit$ = new EventEmitter();
   }

  ngOnInit() {
    this.form = this.fb.group({
			title: [this.data.record.title, Validators.required],
      description: [this.data.record.description],
      			
		});
  }

  onDialogSubmit(){
    console.log(this.form.value);
    this.formSubmit$.emit(true);
    this.updateDialogRef.close(true);
  }


}
