﻿export class BranchResponse {
    
public result: BranchResult;
public targetUrl : string;
public success : boolean;
public error : string;
public unAuthorizedRequest: boolean;
public __abp: boolean;

}
export class BranchResult
{
    public totalCount : number;
    public items : Branch[]

}
export class Branch
{
    tenantId: number;
    countryId: number;
    stateId: number;
    name: string;
    region: string;
    numberOfSchools: number;
    numberOfEducators: number;
    numberOfStudents: number;
    numberOfParents: number;
    primaryContactName: string;
    primaryContactEmail: string;
    primaryContactPhoneNumber: string;
    id: string;
    
}


export class BranchPost {
    countryId: number;
    stateId: number;
    name: string;
    region: string;
    numberOfSchools: number;
    numberOfEducators: number;
    numberOfStudents: number;
    numberOfParents: number;
    primaryContactName: string;
    primaryContactEmail: string;
    primaryContactPhoneNumber: string;
    id:string;
}
    


// export class User {
//     userName: string;
//     name: string;
//     surname: string;
//     emailAddress: string;
//     isActive: string;
//     fullName: string;
//     lastLoginTime: string;
//     creationTime: string;
//     roleNames: string[];
//     id:number    
// }



