﻿export class EvaluationResponse {
    
public result: Evaluation[];
public targetUrl : string;
public success : boolean;
public error : string;
public unAuthorizedRequest: boolean;
public __abp: boolean;

}
export class Evaluation
{
    public branchId : string;
    public classroomId : string;
    public contentId : string;
    public name : string;
    public status : number;
    public userEvaluations : string;
    public id : string;
    public items : Questions[];
    public branchName:string;
    public classRoomName:string;

}
export class Questions
{
    public evaluationId : string;
    public questionType : number;
    public description : string;
    public value : number;
    public position : number;
    public id : string;
    public questionOptions : QuestionOptions[];
    public AllowedMultiple:boolean;
}

export class QuestionOptions
{
    public evaluationQuestionId : string;
    public description : string;
    public position : number;
    public fileAttachments : string[];
    public id : string;
    public isCorrect: boolean;
    public imagePath:string;

}
export class EvaluationPost {
    public id : string;
    public BranchId: string;
    public classroomId: string;
    public contentId: string;
    public name: string;
    public questions: Questions[];
}


