﻿
export class GradeResponse {

    public result: GradeResult;
    public targetUrl: string;
    public success: boolean;
    public error: string;
    public unAuthorizedRequest: boolean;
    public __abp: boolean;

}
export class GradeResult {
    public totalCount: number;
    public items: Grade[]

}
export class Grade {
    branchId: string;
    name: string;
    description: string;
    id: string;


}
export class GradeOptions {
    name: string;
    value: string;
    checked: boolean;

}
