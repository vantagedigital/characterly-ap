﻿export class SubjectResponse {

    public result: Subject[];
    public targetUrl: string;
    public success: boolean;
    public error: string;
    public unAuthorizedRequest: boolean;
    public __abp: boolean;

}

export class Subject {
    branchId: string;
    name: string;
    description: string;
    id: string;


}
export class SubjectOptions {
    name: string;
    value: string;
    checked: boolean;

}
