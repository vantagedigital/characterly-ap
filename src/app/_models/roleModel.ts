
export class RoleModel {
    id?: string;
    Name: string;
    DisplayName: string;
    Description: string;
    IsStatic: boolean;
    Permissions: string[];
}
