﻿export class ContentResponse {
    
public result: Content[];
public targetUrl : string;
public success : boolean;
public error : string;
public unAuthorizedRequest: boolean;
public __abp: boolean;

}
export class Content
{
    public branchId : string;
    public language : string;
    public countryId : string;
    public status : number;
    public title : string;
    public description : string;
    public contentText : string;
    public footPrint : string
    public contentType : number;
    public contentTags : string[];
    public domains : Domain[];
    public contentSchedules : string[];
    public pillars : Pillar[];
    public subjects : Subject[];
    public grades : Grade[];
    public fileAttachments : Questions[]
    public evaluations : Evaluation[];
    public id : string

}
export class Domain
{
    public parentDomainId : string;
    public code : string;
    public name : string;
    public domainCategory : number;
    public id : string;


}

export class Pillar
{
    public branchId : string;
    public name : string;
    public description : string;
    public colorCode : string;
    public images : string[];
    public id : string;
    

}

export class Subject
{
    public branchId : string;
    public name : string;
    public description : string;
    public id : string;
    

}

export class Grade
{
    public branchId : string;
    public name : string;
    public description : string;
    public id : string;
    

}


export class Evaluation
{
    public branchId : string;
    public classroomId : string;
    public contentId : string;
    public name : string;
    public status : number;
    public userEvaluations : string;
    public id : string;
    public items : Questions[]

}
export class Questions
{
    public evaluationId : string;
    public questionType : number;
    public description : string;
    public value : number;
    public position : number;
    public id : string;
    public questionOptions : QuestionOptions[];

}

export class QuestionOptions
{
    public evaluationQuestionId : string;
    public description : string;
    public position : number;
    public fileAttachments : string[];
    public id : string;
    

}


export class ContentPost {
    public id : string;
    public branchId: string;
    public language: string;
    public countryId: number;
    public status: number;
    public title: string;

    public description: string;
    public footPrint: string;
    public contentType: number;
    public contentText:string;
    public contentTags: string[];


    public domainIds=[];
    public pillarIds: string[];
    public subjectIds: string[];
    public gradeIds: string[];
    public contentPages:any[];

}

export enum ContentType {
    Article = 0,
    Audio = 1,
    Image = 2,
    Video = 3,
    Quote = 4,
    FlashCard = 5
}


export enum ProductTypeEnum {
    Book = 1,
    Toy,
    Music
}






