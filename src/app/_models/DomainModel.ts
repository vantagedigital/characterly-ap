﻿export class DomainResponse {

    public result: DomainResult;
    public targetUrl: string;
    public success: boolean;
    public error: string;
    public unAuthorizedRequest: boolean;
    public __abp: boolean;

}
export class DomainResult {
    public totalCount: number;
    public items: Domain[]

}
export class Domain {
    parentDomainId: string;
    code: string;
    name: string;
    domainCategory: number;
    id: string;
}

export class DomainChildList {
    parentDomainId: string;
    code: string;
    name: string;
    domainCategory: number;
    id: string;
    childDomain:DomainChildList[]=[];
    checked:boolean;
}

export class DomainOptions {
    name: string;
    value: string;
    checked: boolean;

}


export class DomainPost {
    countryId: number;
    stateId: number;
    name: string;
    region: string;
    numberOfSchools: number;
    numberOfEducators: number;
    numberOfStudents: number;
    numberOfParents: number;
    primaryContactName: string;
    primaryContactEmail: string;
    primaryContactPhoneNumber: string;
}




// export class User {
//     userName: string;
//     name: string;
//     surname: string;
//     emailAddress: string;
//     isActive: string;
//     fullName: string;
//     lastLoginTime: string;
//     creationTime: string;
//     roleNames: string[];
//     id:number    
// }



