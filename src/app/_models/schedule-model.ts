
export class ScheduleResponse {
    public _body: Schedule[];
    public result: Schedule[];
    public targetUrl: string;
    public success: boolean;
    public error: string;
    public unAuthorizedRequest: boolean;
    public __abp: boolean;

}
export class Schedule {
    public id: string;
    public title: string;
    public start: string;
    public end: string;

}

export class ScheduleEvent {
    public title: string;
    public start: string;
    public end: string;
    public description: string;
}
export enum months {
    January = 1,
    February = 2,
    March = 3,
    April = 4,
    May = 5,
    June = 6,
    July = 7,
    August = 8,
    September = 9,
    October = 10,
    November = 11,
    December = 12
}