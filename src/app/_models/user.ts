﻿export class UserResponse {
    
public result: UserResult;
public targetUrl : string;
public success : boolean;
public error : string;
public unAuthorizedRequest: boolean;
public __abp: boolean;

}
export class UserResult
{
    public totalCount : number;
    public items : User[]

}
export class User
{
        userName: string;
    name: string;
    surname: string;
    emailAddress: string;
    gender:string;
    isActive: string;
    fullName: string;
    lastLoginTime: string;
    creationTime: string;
    roleNames: string[];
    id:number    
}

export class UserPost {
    id:string;
    BranchId: string;
    UserName: string;
    Name: string;
    Surname: string;
    EmailAddress: string;
    IsActive: boolean;
    roleNames: string[];
    Password: string;
    profilePicture: string;
    gender:number;
    birthdate:Date;
}



