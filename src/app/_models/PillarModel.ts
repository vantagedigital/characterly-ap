﻿export class PillarResponse {

    public result: Pillar[];
    public targetUrl: string;
    public success: boolean;
    public error: string;
    public unAuthorizedRequest: boolean;
    public __abp: boolean;

}

export class Pillar {
    branchId: string;
    name: string;
    description: string;
    domainCategory: number;
    colorCode: string;
    image: string;
    id: string;


}


export class PillarOptions {
    name: string;
    value: string;
    checked: boolean;

}


//export class DomainOptions {
//    name: string;
//    value: string;
//    checked: boolean;

//}


//export class DomainPost {
//    countryId: number;
//    stateId: number;
//    name: string;
//    region: string;
//    numberOfSchools: number;
//    numberOfEducators: number;
//    numberOfStudents: number;
//    numberOfParents: number;
//    primaryContactName: string;
//    primaryContactEmail: string;
//    primaryContactPhoneNumber: string;
//}




// export class User {
//     userName: string;
//     name: string;
//     surname: string;
//     emailAddress: string;
//     isActive: string;
//     fullName: string;
//     lastLoginTime: string;
//     creationTime: string;
//     roleNames: string[];
//     id:number    
// }



