﻿import { Component } from '@angular/core';

//import { LocalStorage } from "angular2-localstorage/WebStorage";
//import { AppCheckComponent } from './check.component';
declare var google:any;

@Component ({
   selector: 'my-app',
   templateUrl: 'page_01.html',
})

export class AppOverview  {
  ngOnInit() {
        setTimeout(() => {
          google.charts.load("current", {packages:["corechart"]});
              google.charts.setOnLoadCallback(pillarChart)
              google.charts.setOnLoadCallback(formatChart)
              google.charts.setOnLoadCallback(subjectChart)
              google.charts.setOnLoadCallback(domainsChart)

                  function pillarChart() {
                  var data = google.visualization.arrayToDataTable([
                    ['Task', 'Pillars'],
                    ['Trustworthiness', 10],
                    ['Respect',         10],
                    ['Responsibility',  10],
                    ['Fairness',        10],
                    ['Caring',          10],
                    ['Citizenship',     10],
                    ['introduction',    10]
                  ]);

                  var options = {
                      pieHole: 0.4,
                      colors: ['#0C4599', '#FFBF00', '#009145', '#FC7B28', '#E82A16', '#924094', '#6b7c93'],
                      chartArea:{left:10,top:10,width:'100%',height:'100%'}
                  };

                  var chart = new google.visualization.PieChart(document.getElementById('pillars'));
                  chart.draw(data, options);
                  }

                  function formatChart() {
                  var data = google.visualization.arrayToDataTable([
                    ['Task', 'Format'],
                    ['Articles',     10],
                    ['Videos',       10],
                    ['Audio',        10],
                    ['Images',       10],
                    ['Flash card',   10],
                    ['Quotes',       10]
                  ]);

                  var options = {
                      pieHole: 0.4,
                      colors: ['#0C4599', '#FFBF00', '#009145', '#FC7B28', '#E82A16', '#924094', '#924094'],
                      chartArea:{left:10,top:10,width:'100%',height:'100%'}
                  };

                  var chart = new google.visualization.PieChart(document.getElementById('format'));
                  chart.draw(data, options);
                  }

                  function subjectChart() {
                  var data = google.visualization.arrayToDataTable([
                    ['Task', 'Subject'],
                    ['English',        12.5],
                    ['Language Arts',  12.5],
                    ['Math',           12.5],
                    ['Physical Edu',   12.5],
                    ['Religion',       12.5],
                    ['Science',        12.5],
                    ['Social Studies', 12.5],
                    ['Spanish',        12.5]
                  ]);

                  var options = {
                      pieHole: 0.4,
                      colors: ['#5d295f', '#6f3170', '#803882', '#924094', '#a448a6', '#b253b4', '#ba64bc', '#c173c3'],
                      chartArea:{left:10,top:10,width:'100%',height:'100%'}
                  };

                  var chart = new google.visualization.PieChart(document.getElementById('subject'));
                  chart.draw(data, options);
                  }

                  function domainsChart() {
                  var data = google.visualization.arrayToDataTable([
                    ['Task', 'Subject'],
                    ['Social Emotional',     33.3],
                    ['Character / Moral',    33.3],
                    ['Academic',             33.3]
                  ]);

                  var options = {
                      pieHole: 0.4,
                      colors: ['#0C4599', '#FFBF00', '#009145'],
                      chartArea:{left:10,top:10,width:'100%',height:'100%'}
                  };

                  var chart = new google.visualization.PieChart(document.getElementById('domains'));
                  chart.draw(data, options);
                }
        }, 0);
    }





}
