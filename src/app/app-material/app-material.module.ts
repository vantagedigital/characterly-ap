﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MdToolbarModule,
    
} from '@angular2-material/toolbar';

@NgModule({
    imports: [CommonModule],
    exports: [
        MdToolbarModule,
       
    ]
})
export class AppMaterialModule { }