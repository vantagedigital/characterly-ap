import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import {AuthenticationService} from './authentication.service';
@Injectable()
export class AccountService {
  private controllerUrl: string;
  constructor(
      private request: RequestService,
      private authenticationService:AuthenticationService) {
      this.controllerUrl = "services/app/Account/";
  }

  public ChangePassword(emailAddress) {
      let data = {'emailAddress':emailAddress};
      
      let url = this.controllerUrl + "ResetPasswordRequest";
      return this.request.CreatePost(data,url);
  }
  public RecoverPassword(emailAddress:string, password:string,token:string) {
    let data = {'emailAddress':emailAddress,
                'token':token,
                'password':password};
    
    let url = this.controllerUrl + "ResetPassword";
    return this.request.CreatePost(data,url);
  }
public RecoverCode(email) {
    let data = {'email':email};
    
    let url = this.controllerUrl + "RecoverAccessCode";
    return this.request.CreatePost(data,url);
}
  public RegistrUser(user) {
    //let data = {'emailAddress':emailAddress};
    
    let url = this.controllerUrl + "Register";
    return this.request.CreatePost(user,url);
}
}
