﻿import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class SubjectService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Subject/";
    }

    public GetSubjectsList() {
        let url = this.controllerUrl + "GetAll";
        return this.request.GetList(url);
    }

    public GetSubjectListByBranchId(branchId:string) {
        let url = this.controllerUrl + "GetByBranchId" + "?id=" + branchId;
        return this.request.GetList(url);
    }


}