import { async } from '@angular/core/testing';
import { GeneralList } from './../_models/general-code';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { RequestHelperService } from './request-helper.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RequestService {
  data: any;
  baseURL: string;
  constructor(
    private http: Http,
    private authenticationService: AuthenticationService,
    private reqHelp: RequestHelperService) {
    // set token if saved in local storage
    // var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    // this.token = currentUser.token;
    this.baseURL = this.reqHelp.baseURL();
  }


  public GetList(controllerUrl: string) {
    let url = this.baseURL + controllerUrl;
    let headers = new Headers({ 'Accept': 'application/json, text/json, text/plain', 'Authorization': 'Bearer ' + this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });    
    return this.http.get(url, options)
      .map(this.extractData)
      .do(val => {
        this.data = val;
        // when the cached data is available we don't need the `Observable` reference anymore

      })
      // make it shared so more than one subscriber can get the result
      .share();
  }

  public DeletePost(controllerUrl: string) {
    let url = this.baseURL + controllerUrl;
    let headers = new Headers({ 'Accept': 'application/json, text/json, text/plain', 'Authorization': 'Bearer ' + this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    
    return this.http.delete(url, options)
      .map(this.extractData)
      .do(val => {
        this.data = val;
        // when the cached data is available we don't need the `Observable` reference anymore

      })
      // make it shared so more than one subscriber can get the result
      .share();
  }

  public async GetListSlow(controllerUrl: string) {
    let url = this.baseURL + controllerUrl;
    let headers = new Headers({ 'Accept': 'application/json, text/json, text/plain', 'Authorization': 'Bearer ' + this.authenticationService.token });
    let options = new RequestOptions({ headers: headers });
    
    return await this.http.get(url, options).toPromise();
     
  }

  public CreatePost(requestData: any, controllerUrl: any) {
    
    let url = this.baseURL + controllerUrl;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Accept', 'application/json, text/json, text/plain');
    headers.append('Authorization', 'Bearer ' + this.authenticationService.token);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, JSON.stringify(requestData), options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  public UpdatePost(requestData: any, controllerUrl: any) {
    
    let url = this.baseURL + controllerUrl;
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Accept', 'application/json, text/json, text/plain');
    headers.append('Authorization', 'Bearer ' + this.authenticationService.token);
    let options = new RequestOptions({ headers: headers });
    return this.http.put(url, JSON.stringify(requestData), options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  public UploadRequest(requestData: any, controllerUrl: any) {
    console.log(requestData);
    let url = this.baseURL + controllerUrl;
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + this.authenticationService.token);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, requestData, options)
      .map(this.extractData)
      .catch(this.handleErrorObservable);
  }

  private extractData(res: Response) {
    let body = res.json();   
    return body || {};
  }

  //  getUser(id: number | string) {
  //     return this.GetEvaluationList()
  //       // (+) before `id` turns the string into a number
  //       .map(heroes => heroes.result.items.find(hero => hero.id === +id));
  //   }

  private handleErrorObservable(error: Response | any) {    
    return Observable.throw(error.message || error);
  }

}
