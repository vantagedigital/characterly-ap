﻿import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { DomainPost } from '../_models/DomainModel';

@Injectable()
export class DomainService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Domain/";
    }


    public GetDomainsList() {
        let url = this.controllerUrl + "GetAll";
        return this.request.GetList(url);
    }

    public CreateBranch(domainObj: DomainPost) {
        let url = this.controllerUrl + "Create";
        return this.request.CreatePost(domainObj, url);
    }

}