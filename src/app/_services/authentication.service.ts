﻿import { RequestHelperService } from './request-helper.service';
import { RequestService } from './request.service';
import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

var headers = new Headers();


// headers.append("withCredentials", "true");


@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(
        private http: Http,
        private requestHelp: RequestHelperService) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;

        headers.append("Access-Control-Allow-Origin", "*");
headers.append("Access-Control-Allow-Methods", "POST, GET, DELETE");
headers.append("Accept", "application/json, text/json, text/plain");
headers.append("Content-Type", "application/json");
    }

    login(username: string, password: string): Observable<boolean> {
        localStorage.setItem('currentUser', JSON.stringify({ username: 'User Name', token: 'session-token' }));

        //return Observable.of(true);
        //return this.http.options(this.requestHelp.baseURL()+'TokenAuth/Authenticate', JSON.stringify({ userNameOrEmailAddress: username, password: password, rememberClient: "false" }))
        return this.http.post(this.requestHelp.baseURL()+'TokenAuth/Authenticate', JSON.stringify({ userNameOrEmailAddress: username, password: password, rememberClient: "false" }), { headers: headers })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.json().result.accessToken;
                if (token) {
                    // set token property
                    this.token = token;
                    localStorage.setItem('userId',response.json().result.userId);
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            }).catch(this.handleErrorObservable);
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        
        this.token = null;
        localStorage.removeItem('currentUser');
    }

    private handleErrorObservable(error: Response | any) {
        debugger;
        //alert(error.json().error.message + error.json().error.details);
        //return Observable.throw(error.json().error.message || error.json().error.message);
        return Observable.of(false);
    }
}
