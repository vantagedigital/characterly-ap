import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { EvaluationPost } from '../_models/EvaluationModel';


@Injectable()
export class ClassRoomService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Classroom/";
    }


    public GetClassRoomList() {
        let url = this.controllerUrl + "GetAll";
        return this.request.GetList(url);
    }

 
}