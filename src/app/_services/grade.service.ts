﻿import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class GradeService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Grade/";
    }
    
    public GetGradesList() {
        let url = this.controllerUrl + "GetAll";
        return this.request.GetList(url);
    }
    public GetGradesListByBranchId(branchId:string) {
        let url = this.controllerUrl + "GetByBranchId" + "?id=" + branchId;
        return this.request.GetList(url);
    }
}