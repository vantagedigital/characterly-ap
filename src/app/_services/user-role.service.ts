import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class UserRoleService {
  private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Role/";
    }


    public GetRolesList(param=null) {
        if (param==null) param={};
        param.searchText = param.searchText ? param.searchText : '';
        param.skipCount = param.skipCount ? param.skipCount : 0;
        param.maxResultCount = param.maxResultCount ? param.maxResultCount : 50;
        param.sortOrder = param.sortOrder ? param.sortOrder : 1;

        let url = this.controllerUrl + `GetAll?SkipCount=${param.skipCount}&MaxResultCount=${ param.maxResultCount}`;
        
        //let url = `${this.controllerUrl}Search?searchText=${param.searchText}&sortOrder=${param.sortOrder}&skipCount=${param.skipCount}&maxResultCount=${param.maxResultCount}`;

        return this.request.GetList(url);
    }
  
    // public CreateEvaluation(evaluationObj: EvaluationPost){
    //     let url=this.controllerUrl + "Create";
    //     return this.request.CreatePost(evaluationObj,url);
    // }
}
