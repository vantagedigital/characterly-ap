﻿import { Injectable } from '@angular/core';
import { BranchPost } from '../_models/BranchModel';
import { RequestService } from './request.service';

@Injectable()
export class BranchService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Branch/";
    }

    public GetBranchesList() {
        let url = this.controllerUrl + "GetAll";
        return this.request.GetList(url);
    }

    public getBranches(skipCount: number, resultCount: number) {
        let url = this.controllerUrl + `GetAll?SkipCount=${skipCount}&MaxResultCount=${resultCount}`;
        return this.request.GetList(url);
    }

    public getSearchBranches(param=null) {
        /*"{\r\n  \"searchText\": \"\",\r\n  \"sortOrder\": 1,\r\n  \"status\": null,\r\n  \"type\":null,\r\n  \"gradeIds\": [],\r\n  \"pillarIds\": [],\r\n  \"subjectIds\": [],\r\n  \"skipCount\": 0,\r\n  \"maxResultCount\": 20\r\n}"*/
        param.searchText = param.searchText ? param.searchText : '';
        param.skipCount = param.skipCount ? param.skipCount : 0;
        param.maxResultCount = param.maxResultCount ? param.maxResultCount : 50;
        param.sortOrder = param.sortOrder ? param.sortOrder : 1;

        let url = this.controllerUrl + `GetAll?SkipCount=${param.skipCount}&MaxResultCount=${ param.maxResultCount}`;
        
        //let url = `${this.controllerUrl}Search?searchText=${param.searchText}&sortOrder=${param.sortOrder}&//skipCount=${param.skipCount}&maxResultCount=${param.skipCount}`;

        return this.request.GetList(url);

    }    

    public CreateBranch(branchObj: BranchPost){
        let url=this.controllerUrl + "Create";
        return this.request.CreatePost(branchObj,url);
    }

    public UpdateBranch(branchObj: BranchPost){
        let url=this.controllerUrl + "Update";
        return this.request.UpdatePost(branchObj,url);
    }

    public DeleteBranch(id:any) {
        let url = this.controllerUrl + "Delete?Id="+id;
        return this.request.DeletePost(url);
    }
    public GetBranchById(id:any) {
        let url = this.controllerUrl + "Get?Id="+id;
        return this.request.GetList(url);
    }
    getBranch(id: number | string) {
        return this.GetBranchesList()
            // (+) before `id` turns the string into a number
            .map(heroes => heroes.result.items.find(hero => hero.id === +id));
    }
}