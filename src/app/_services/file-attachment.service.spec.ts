import { TestBed, inject } from '@angular/core/testing';

import { FileAttachmentService } from './file-attachment.service';

describe('FileAttachmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileAttachmentService]
    });
  });

  it('should be created', inject([FileAttachmentService], (service: FileAttachmentService) => {
    expect(service).toBeTruthy();
  }));
});
