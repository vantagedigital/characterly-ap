import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
@Injectable()
export class NotificationsService {

  private controllerUrl: string;
  constructor(
    private request: RequestService) {
    this.controllerUrl = "services/app/SystemNotification/";
  }

  public GetAllNotifications() {
    let url = this.controllerUrl + "GetMyNotifications";
    return this.request.GetList(url);
  }

}
