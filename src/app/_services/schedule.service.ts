import { Injectable } from '@angular/core';
import { RequestService } from './request.service';


@Injectable()
export class ScheduleService {
  private controllerUrl: string;
  constructor(
    private request: RequestService) {
    this.controllerUrl = "services/app/ContentSchedule/";
  }


  public GetScheduleList() {
    let url = this.controllerUrl + "GetAll?MaxResultCount=500";
    return this.request.GetList(url);
  }
  public createSchedule(data) {
    let url = this.controllerUrl + "Create";
    return this.request.CreatePost(data,url);
  }
  public updateSchedule(data) {
    let url = this.controllerUrl + "Update";
    return this.request.UpdatePost(data,url);    
  }
}
