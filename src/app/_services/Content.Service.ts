﻿import { async } from '@angular/core/testing';
import { ContentPost } from './../_models/ContentModel';
import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { GeneralList } from '../_models/general-code';

@Injectable()
export class ContentService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Content/";
    }

    public GetAllGlobal() {
        let url = this.controllerUrl + `Search`;
        return this.request.GetList(url); 
    }
    
    public getContents(skipCount: number, resultCount: number) {
        let url = this.controllerUrl + `GetAll?SkipCount=${skipCount}&MaxResultCount=${resultCount}`;
        return this.request.GetList(url);
    }

    public getSearchContents(param=null) {
        /*"{\r\n  \"searchText\": \"\",\r\n  \"sortOrder\": 1,\r\n  \"status\": null,\r\n  \"type\":null,\r\n  \"gradeIds\": [],\r\n  \"pillarIds\": [],\r\n  \"subjectIds\": [],\r\n  \"skipCount\": 0,\r\n  \"maxResultCount\": 20\r\n}"*/
        param.searchText = param.searchText ? param.searchText : '';
        param.skipCount = param.skipCount ? param.skipCount : 0;
        param.maxResultCount = param.maxResultCount ? param.maxResultCount : 50;
        param.sortOrder = param.sortOrder ? param.sortOrder : 1;

        let url = this.controllerUrl + `GetAll?SkipCount=${param.skipCount}&MaxResultCount=${ param.maxResultCount}`;
        
        //let url = `${this.controllerUrl}Search?searchText=${param.searchText}&sortOrder=${param.sortOrder}&skipCount=${param.skipCount}&maxResultCount=${param.maxResultCount}`;

        return this.request.GetList(url);

    }

    public async GetContentListSlow(){
        let url = this.controllerUrl + "GetAll";
        return await this.request.GetListSlow(url);
    }

    public CreateContent(contentObj: ContentPost){
        console.log(ContentPost)
        let url=this.controllerUrl + "Create";
        return this.request.CreatePost(contentObj,url);
    }

    public GetContent(id) {
        let url = this.controllerUrl + "Get?Id="+id;
        return this.request.GetList(url);
    }
    public UpdateContent(contentObj: ContentPost){
        let url=this.controllerUrl + "Update";
        return this.request.UpdatePost(contentObj,url);
    }
    public DeleteContent(contentId) {   
        let url = this.controllerUrl + "Delete?Id="+contentId;
        return this.request.DeletePost(url);
    }
}