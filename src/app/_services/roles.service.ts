import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class RolesService {
  private controllerUrl: string;
  constructor(
    private request: RequestService) {
    this.controllerUrl = "services/app/Role/";
  }


  public GetAllPermissions() {
    let url = this.controllerUrl + "GetAllPermissions";
    return this.request.GetList(url);
  }

  public CreateRole(data) {
    let url = this.controllerUrl + "Create";
    return this.request.CreatePost(data, url);
  }
  public UpdateRole(data) {
    let url = this.controllerUrl + "Update";
    return this.request.UpdatePost(data, url);
  }
  public DeleteRole(Id) {
    let url = this.controllerUrl + "Delete?Id="+Id;
    return this.request.DeletePost(url);
  }

}
