﻿import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { EvaluationPost } from '../_models/EvaluationModel';


@Injectable()
export class EvaluationService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Evaluation/";
    }


    public getEvaluations(skipCount: number, resultCount: number) {
        let url = this.controllerUrl + `GetAll?SkipCount=${skipCount}&MaxResultCount=${resultCount}`;
        return this.request.GetList(url);
    }

    public getSearchEvaluations(param=null) {
        /*"{\r\n  \"searchText\": \"\",\r\n  \"sortOrder\": 1,\r\n  \"status\": null,\r\n  \"type\":null,\r\n  \"gradeIds\": [],\r\n  \"pillarIds\": [],\r\n  \"subjectIds\": [],\r\n  \"skipCount\": 0,\r\n  \"maxResultCount\": 20\r\n}"*/
        param.searchText = param.searchText ? param.searchText : '';
        param.skipCount = param.skipCount ? param.skipCount : 0;
        param.maxResultCount = param.maxResultCount ? param.maxResultCount : 50;
        param.sortOrder = param.sortOrder ? param.sortOrder : 1;

        let url = this.controllerUrl + `GetAll?SkipCount=${param.skipCount}&MaxResultCount=${ param.maxResultCount}`;
        
        //let url = `${this.controllerUrl}Search?searchText=${param.searchText}&sortOrder=${param.sortOrder}&//skipCount=${param.skipCount}&maxResultCount=${param.skipCount}`;

        return this.request.GetList(url);

    }


    public CreateEvaluation(evaluationObj: EvaluationPost){
        let url=this.controllerUrl + "Create";
        return this.request.CreatePost(evaluationObj,url);
    }

    public GetEvaluation(id) {
        let url = this.controllerUrl + "Get?id="+id;
        return this.request.GetList(url);
    }

    public DeleteEvaluation(evaluationId) {   
        let url = this.controllerUrl + "Delete?Id="+evaluationId;
        return this.request.DeletePost(url);
    }

    public UpdateEvaluation(evaluationObj: EvaluationPost){
        let url=this.controllerUrl + "Update";
        return this.request.UpdatePost(evaluationObj,url);
    }
    public DeleteEvaluationByUser(evaluationId) {
        let userId= localStorage.getItem('userId');
        let url = this.controllerUrl + "RemoveFromUser?evaluationId="+evaluationId+"&userId="+userId;
        return this.request.DeletePost(url);
    }
    public DeleteEvaluationByClass(evaluationId,classId) {
        let userId= localStorage.getItem('userId');
        let url = this.controllerUrl + "RemoveFromClassroom?evaluationId="+evaluationId+"&classroomId="+classId;
        return this.request.DeletePost(url);
    }
}