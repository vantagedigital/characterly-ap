import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class CountryService {
public controllerUrl:string;
  constructor(
    private request:RequestService
  ) {
    this.controllerUrl="services/app/Country/";
   }

   getAll(){
     let url=this.controllerUrl+"GetAll";
     return this.request.GetList(url);
     
   }

}
