﻿import { Injectable } from '@angular/core';
import { UserPost } from '../_models/user';
import { RequestService } from './request.service';

@Injectable()
export class UserServices {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/User/";
    }

    public GetUserListCall() {
        let url = this.controllerUrl + "GetAll?SkipCount=0&MaxResultCount=50";
        return this.request.GetList(url);
    }

    public getUsers(skipCount: number, resultCount: number) {
        let url = this.controllerUrl + `GetAll?SkipCount=${skipCount}&MaxResultCount=${resultCount}`;
        return this.request.GetList(url);
    }

    public getSearchUsers(param=null) {
        /*"{\r\n  \"searchText\": \"\",\r\n  \"sortOrder\": 1,\r\n  \"status\": null,\r\n  \"type\":null,\r\n  \"gradeIds\": [],\r\n  \"pillarIds\": [],\r\n  \"subjectIds\": [],\r\n  \"skipCount\": 0,\r\n  \"maxResultCount\": 20\r\n}"*/
        if (param==null) param={};
        param.searchText = param.searchText ? param.searchText : '';
        param.skipCount = param.skipCount ? param.skipCount : 0;
        param.maxResultCount = param.maxResultCount ? param.maxResultCount : 50;
        param.sortOrder = param.sortOrder ? param.sortOrder : 1;

        let url = this.controllerUrl + `GetAll?SkipCount=${param.skipCount}&MaxResultCount=${ param.maxResultCount}`;
        
        //let url = `${this.controllerUrl}Search?searchText=${param.searchText}&sortOrder=${param.sortOrder}&//skipCount=${param.skipCount}&maxResultCount=${param.skipCount}`;

        return this.request.GetList(url);

    }    

    public CreateUser(userObj: UserPost){
        let url=this.controllerUrl + "Create";
        return this.request.CreatePost(userObj,url);
    }
    public UpdateUser(userObj: UserPost){
        let url=this.controllerUrl + "Update";
        return this.request.UpdatePost(userObj,url);
    }
    public GetUserById(id:any) {
        let url = this.controllerUrl + "Get?Id="+id;
        return this.request.GetList(url);
    }
    public DeleteUser(id:any) {
        let url = this.controllerUrl + "Delete?Id="+id;
        return this.request.DeletePost(url);
    }
    getUser(id: number | string) {
        return this.GetUserListCall()
            // (+) before `id` turns the string into a number
            .map(heroes => heroes.result.items.find(hero => hero.id === +id));
    }

}