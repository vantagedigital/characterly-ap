import { TestBed, inject } from '@angular/core/testing';

import { LazyloadService } from './lazyload.service';

describe('LazyloadService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LazyloadService]
    });
  });

  it('should be created', inject([LazyloadService], (service: LazyloadService) => {
    expect(service).toBeTruthy();
  }));
});
