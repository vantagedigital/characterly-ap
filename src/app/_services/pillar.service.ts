﻿import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class PillarService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/Pillar/";
    }

    public GetPillarsList() {
        let url = this.controllerUrl + "GetAll";
        return this.request.GetList(url);
    }
    public GetPillarsListByBranchId(branchId:string) {
        let url = this.controllerUrl + "GetByBranchId" + "?id=" + branchId;
        return this.request.GetList(url);
    }


}