import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class RequestHelperService {

  baseURL(){
    //return "http://13.92.193.213:80/api/";
    return environment.apiUrl; //"http://betaapi.character.ly/api/";
    
  }
  currrentUser(){
    return JSON.parse(localStorage.getItem('currentUser'));
  }
 
  constructor() { }

}
