import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class FileAttachmentService {
  private controllerUrl: string;
  constructor(
      private request: RequestService) {
      this.controllerUrl = "services/app/FileAttachment/";
  }
  public UploadFile(data){
    let url = this.controllerUrl + "Upload";
    //return this.request.CreatePost(data,url);
    return this.request.UploadRequest(data, url);
  }
  public UpdateAttachment(data){
    let url = this.controllerUrl + "Update";
    return this.request.UpdatePost(data,url);
  }

  public GetByEntityId(id){
    let url = this.controllerUrl + "GetByEntityId?Id="+id;
    return this.request.GetList(url);
  }

  public Delete(id){
    let url = this.controllerUrl + "Delete?Id="+id;
    return this.request.DeletePost(url);
  }

}
