﻿import { Injectable } from '@angular/core';
import { RequestService } from './request.service';

@Injectable()
export class UserService {
    private controllerUrl: string;
    constructor(
        private request: RequestService) {
        this.controllerUrl = "services/app/User/";
    }

    getUsers() {
        let url = this.controllerUrl + "GetAll?SkipCount=0&MaxResultCount=10";
        return this.request.GetList(url);
    }

}