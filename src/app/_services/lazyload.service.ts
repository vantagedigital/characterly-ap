import { Injectable } from '@angular/core';

@Injectable()
export class LazyloadService {

  constructor() { }
  lazyloadByMiliSec(miliSec:any){
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
      if ((new Date().getTime() - start) > +miliSec) {
        break;
      }
    }
  }
}
