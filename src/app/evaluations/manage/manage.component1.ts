import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';


import { EvaluationService, BranchService, ClassRoomService } from '../../_services/index';
import { EvaluationResponse } from '../../_models/EvaluationModel';
import { Evaluation } from '../../_models/EvaluationModel';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';

const DEFAULT_PAGE_SIZE = 5;

@Component({
  selector: 'app-evaluations-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['manage.component.css']
})


export class AppEvaluationsManageComponent {
  successMessage ='';
  EvaluationFullResponse: EvaluationResponse;
  evaluations: Evaluation[]=[];
  selectedEvalutaions: Evaluation[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
	filter:string;
	sort:string;
	dir: string;

  loading = false;
  error = '';

  loadingIndicator = true;
  reorderable = true;

  searchControl = new FormControl();
  componentDestroyed = new Subject(); // Component Destroy
  allBranches=[];
  allClassRooms=[];

  constructor(
    private router: Router,
    private viewContainer: ViewContainerRef,
    private EvaluationService: EvaluationService,
    private EvaluationResponse: EvaluationResponse,
    private branchService:BranchService,
    private classRoomService:ClassRoomService,
    private confirmationDialogsService: ConfirmationDialogsService) { }

  ngOnInit() {
    this.getEvaluations();

    this.searchControl.valueChanges
		.debounceTime(300) 
      .distinctUntilChanged()  
      .takeUntil(this.componentDestroyed)
      .subscribe( filter => { 
        this.filter = filter ? filter : '';
        this.getEvaluations();
      });
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  getEvaluations(params=null) {
    this.loadingIndicator = true;
    let query = {
      skipCount: this.pageNumber * this.pageSize,
      maxResultCount: this.pageSize,
      filter: this.filter,
      ...params
    }

    this.EvaluationService.getSearchEvaluations(query).subscribe(
      (data) => {
        this.loadingIndicator = false;
        this.EvaluationFullResponse = data;
        this.evaluations = data.result;
        console.log(data);
        this.getClassRooms();
        this.getBraches();
      }, (err) => {
        this.loadingIndicator = false;
        console.log(err);
      });    
  }

  deleteEvaluation(evaluationId) {
    console.log(evaluationId);
    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Evaluation?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {

        if (result) {
          let evaluation = this.evaluations.find(x => x.id == evaluationId);
          this.EvaluationService.DeleteEvaluation(evaluationId)
            .subscribe(
              responce => {

                this.getEvaluations();

                // this.EvaluationService.DeleteEvaluationByUser(evaluationId)
                //   .subscribe(responce => {
                // });
            });

        }
      });

  }


  deleteSelectedEvalutations() {
    if (this.selectedEvalutaions.length > 0) {
      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Evaluation?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {

          if (result) {
            for (let index = 0; index < this.selectedEvalutaions.length; index++) {
              let id = this.selectedEvalutaions[index];
              this.EvaluationService.DeleteEvaluation(id)
                .subscribe(responce => {
                    this.selectedEvalutaions.splice(index, 1);
                    this.getEvaluations();
                }, (err) => {
                  console.log(err);
                });

            }
          }
        });
    } else {
      this.showMessage('error', "Please select the Evaluation to delete");
    }


  }

  getClassRooms(){
    this.classRoomService.GetClassRoomList().subscribe(res=>{
      this.allClassRooms = res.result;
  
      this.evaluations.forEach(element => {
        let classRoom = this.allClassRooms.find(item => item.id == element.classroomId);
        if(classRoom)
          element.classRoomName = classRoom.name;
      });
    });
  }
  
  getBraches(){
    this.branchService.GetBranchesList().subscribe(res=>{
      this.allBranches = res.result.items;
  
      this.evaluations.forEach(element => {
        let branch =this.allBranches.find(item => item.id == element.branchId);
        element.branchName = branch.name;
      });
    });
  }
  


  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }

  onSelect({ selected }) {
    this.selectedEvalutaions.splice(0, this.selectedEvalutaions.length);
    this.selectedEvalutaions.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
    this.getEvaluations();
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getEvaluations();
  }

  updateFilter(term: string): void {
    this.filter = term;
    this.getEvaluations();
  }

  convertArrayNameToString(arrObj: any[]) {
    let nameArr = arrObj.map( obj => { return obj.name } );
    return nameArr.join(",");
  }


  min(x, y) {
    return Math.min(x, y);
  }
}