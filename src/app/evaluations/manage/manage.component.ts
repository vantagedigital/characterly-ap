import { Component, OnInit, ViewContainerRef, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';


import { EvaluationService, BranchService, ClassRoomService } from '../../_services/index';
import { EvaluationResponse } from '../../_models/EvaluationModel';
import { Evaluation } from '../../_models/EvaluationModel';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgxSpinnerService } from 'ngx-spinner';

const DEFAULT_PAGE_SIZE = 5;
const MAX_COUNT = 1000;

@Component({
  selector: 'app-evaluations-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['manage.component.css']
})


export class AppEvaluationsManageComponent {
  successMessage ='';
  error = '';

  evaluations: Evaluation[]=[];
  filteredEvaluations: Evaluation[]=[];
  selectedEvaluations: Evaluation[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  pageNumber = 0;
  total: number = 0;
  loadingIndicator = false;
  reorderable = true;

  componentDestroyed = new Subject(); // Component Destroy
  allBranches=[];
  allClassRooms=[];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput') search: ElementRef;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private viewContainer: ViewContainerRef,
    private EvaluationService: EvaluationService,
    private EvaluationResponse: EvaluationResponse,
    private branchService:BranchService,
    private classRoomService:ClassRoomService,
    private confirmationDialogsService: ConfirmationDialogsService) { }

  ngOnInit() {
    this.getEvaluations();
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  getEvaluations() {
    this.spinner.show();
    this.EvaluationService.getEvaluations(0, MAX_COUNT).subscribe(
      (data) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
        this.evaluations = data.result;
        this.total = this.evaluations.length;
        this.updateFilter(this.search.nativeElement.value);

        this.getClassRooms();
        this.getBraches();
      }, (err) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
      });

  }

  deleteEvaluation(evaluationId) {
    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Evaluation?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {

        if (result) {
          let evaluation = this.evaluations.find(x => x.id == evaluationId);
          this.EvaluationService.DeleteEvaluation(evaluationId).subscribe(
              response => {
                let index = this.evaluations.findIndex(x=>x.id==evaluationId);
                if (index != -1) {
                  this.evaluations.splice(index, 1);
                  this.updateFilter(this.search.nativeElement.value);
                }
                //this.getEvaluations();
            });

        }
      });

  }


  deleteSelectedEvalutations() {
    if (this.selectedEvaluations.length > 0) {
      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Evaluation?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {
          if (result) {
            for (let index = 0; index < this.selectedEvaluations.length; index++) {
              let id = this.selectedEvaluations[index].id;
              this.EvaluationService.DeleteEvaluation(id).subscribe(
                response => {
                  let index = this.evaluations.findIndex(x=>x.id==id);
                  if (index != -1) {
                    this.evaluations.splice(index, 1);
                    this.updateFilter(this.search.nativeElement.value);
                  }
                }, (err) => {
                  
                });

            }
          }
        });
    } else {
      this.showMessage('error', "Please select the Evaluation to delete");
    }


  }

  getClassRooms(){
    this.classRoomService.GetClassRoomList().subscribe(res=>{
      this.allClassRooms = res.result;
  
      this.evaluations.forEach(element => {
        let classRoom = this.allClassRooms.find(item => item.id == element.classroomId);
        if(classRoom)
          element.classRoomName = classRoom.name;
      });
    });
  }
  
  getBraches(){
    this.branchService.GetBranchesList().subscribe(res=>{
      this.allBranches = res.result.items;
  
      this.evaluations.forEach(element => {
        let branch =this.allBranches.find(item => item.id == element.branchId);
        element.branchName = branch.name;
      });
    });
  }
  


  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }

  onSelect({ selected }) {
    this.selectedEvaluations.splice(0, this.selectedEvaluations.length);
    this.selectedEvaluations.push(...selected);
  }

  onActivate(evt) {
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getEvaluations();
  }

  updateFilter(term: string): void {
    const val = term.toLowerCase();

    const filteredEvaluations = this.evaluations.filter(function (cat) {
        return Object.keys(cat).some(key => {
            return (cat[key] ? cat[key] : '')
                .toString()
                .toLowerCase()
                .indexOf(val) !== -1 || !val;
        })
    });

    this.filteredEvaluations = filteredEvaluations;
    if (this.table) {
        this.table.offset = 0;
    }
  }

  convertArrayNameToString(arrObj: any[]) {
    let nameArr = arrObj.map( obj => { return obj.name } );
    return nameArr.join(",");
  }


  min(x, y) {
    return Math.min(x, y);
  }
}