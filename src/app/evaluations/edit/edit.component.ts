import { async } from '@angular/core/testing';
import { BranchService } from './../../_services/branch.service';
import { DomainService } from './../../_services/domain.service';
import { ContentService } from './../../_services/content.service';
import { FileAttachmentService } from '../../_services/index';
import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { EvaluationPost, QuestionOptions, Questions, Evaluation, EvaluationResponse } from '../../_models/EvaluationModel';
import { DomainOptions, DomainResponse, Domain, DomainResult, DomainChildList } from '../../_models/DomainModel';
import { CompleterItem, CompleterService, CompleterData } from 'ng2-completer';
import { BranchResponse, Branch, BranchResult } from '../../_models/BranchModel';
import { EvaluationService } from '../../_services/Evaluation.service';
import { UserServices } from '../../_services/user.services';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, NgForm, FormGroup, FormControl } from '@angular/forms';
import { GeneralList } from '../../_models/general-code';
import { NgAutocompleteComponent, CreateNewAutocompleteGroup, SelectedAutocompleteItem } from 'ng-auto-complete';
import { promise } from 'selenium-webdriver';
import { Promise } from 'q';

import "../../../assets/js/jquery.tree-multiselect.min.js";
import { Content } from '../../_models/ContentModel';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
declare var jQuery: any;
declare var tinyMCE: any;
declare var Taggle: any;

@Component({
  selector: 'app-evaluations-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class AppEvaluationsEditComponent implements OnInit {
  @ViewChild(NgAutocompleteComponent) public completer: NgAutocompleteComponent;
  @ViewChild('fileInput') fileInput:any;

  // @ViewChild('f1') form: NgForm;
  // @ViewChild('option') option: ElementRef;
  // @ViewChild('branchId') branchId: ElementRef;
  // @ViewChild('content') content: ElementRef;
  // @ViewChild('title') title: ElementRef;

  //   myForm = new FormGroup({
  //     firstName: new FormControl(),
  //     selectedBranchId:new FormControl()
  //  });


  // For auto complete search
  submitted=false;
  successMessage='';
  questionEmpty=false;
  optionSubmitted=false;

  searchTerm: FormControl = new FormControl();
  searchResult: GeneralList[] = [];
  selectedContentId: string;
  selectedContentName:string;
  selectedConent={};
  sword: string;
  displayList: boolean = false;
  flag: boolean;
  options = [];
  arrayoptions = [];

  model: any = {};
  questionModel: Questions;
  optionModel: QuestionOptions;
  @Input() QuestionsInModel: Questions;

  levels: Array<Object> = [
    { num: 0, name: 'AA' },
    { num: 1, name: 'BB' }
  ];

  questionType: number;
  selectedBranchId: string;
  selectedDomainIds = [];
  selectedPillarIds: string[];
  selectedSubjectIds: string[];
  selectedGradeIds: string[];
  selectedLanguage: string;

  AllDomains: string[];

  BranchesFullResponse: BranchResponse;
  AllBranchesWithCount: BranchResult;
  AllBranchesOnly: Branch[];

  //DomainsFullResponse: DomainResponse;
  //AllDomainsWithCount: DomainResult;
  //AllDomainsOnly: Domain[];
  //AllDomainsWithCheckBoxes = [];
  contentDatasource: GeneralList[] = [];

  modeltopost: EvaluationPost;
  EvaluationFullResponse: EvaluationResponse;
  AllEvaluationsOnly: Evaluation[];


  public questionsList: Questions[] = [];
  public optionsList: QuestionOptions[] = [];
  public edited = false;
  public added = false;
  loading = false;
  error = '';
  fullImagePath: string;
  isOpen = false;
  isSingle = false;
  isMultiple = false;
  isTrueFalse=false;
  isImage = false;
  name: string;
  check: number;
  id: string;
  tinymce: any;
  protected searchStr: string;
  protected dataService: CompleterData;
  title: string;
  option: string;
  description: string='';
  isEdit: boolean;
  EditorEvaluation: string;
  EvaluationText: string;
  openFileResponse:any;
  category:string;
  multipleFileResponse=[];
  editor;
  public domainHierarchyList: DomainChildList[] = [];
  fileUploadResponse:any;
  file:any;
  disableAddOption=false;

  contents: any[] = [];
  selectedContents: any="";

  constructor(
    private spinner: NgxSpinnerService,
    private _fb: FormBuilder,
    private router: Router,
    private userService: UserServices,
    private BranchService: BranchService,
    private EvaluationService: EvaluationService,
    private BrancesResponse: BranchResponse,
    private DomainService: DomainService,
    private completerService: CompleterService,
    private DomainResponse: DomainResponse,
    private contentService: ContentService,
    private fileService:FileAttachmentService,
    private _sanitizer: DomSanitizer,
    private param: ActivatedRoute) {

    this.questionModel = new Questions();
    this.optionModel = new QuestionOptions();
    this.arrayoptions = [];
    this.isEdit = false;
    this.getContents();
  }
  ngOnInit() {
    setTimeout(() => {
      //new Taggle('content-tags');
      tinyMCE.init({
          mode: "specific_textareas",
          editor_selector: "mceEditor",
          setup: editor => {
              this.editor = editor;
              editor.on('keyup', () => {
                  this.EvaluationText = editor.getContent();
              });
          },
      });
      function treeOnChange(allSelectedItems, addedItems, removedItems, unselectAllText) {
          console.log("something changed!");
      }
      jQuery("#content-multiselect").treeMultiselect({
          allowBatchSelection: true,
          startCollapsed: true,

          searchable: true,
          sortable: false
      });

  }, 4000);

    this.BranchService.GetBranchesList().subscribe((data) => {
      this.BranchesFullResponse = data;
      this.AllBranchesWithCount = data.result;
      this.AllBranchesOnly = data.result.items;
    }, (err) => {
      console.log(err);
    });

 

    this.contentService.getContents(0, 10)
      .subscribe((data) => {
        for (let index = 0; index < data.result.length; index++) {
          const element = data.result[index];
          let list = new GeneralList;
          list.id = element.id;
          list.Name = element.title;
          this.contentDatasource.push(list);
        }
        this.searchResult = this.contentDatasource;
        //  this.dataService = this.completerService.local(this.contentDatasource , 'title', 'title');

        if (this.id != null) {
          this.EvaluationService.GetEvaluation(this.id)
            .subscribe(responce => {
              debugger;
              console.log(responce);
              this.title = responce.result.name;
              let content:any = this.contentDatasource.find(x => x.id == responce.result.contentId);
              this.selectedContentName = content.Name;
              this.fillTextbox(content);
              this.questionsList = responce.result.questions;
              this.selectedBranchId = responce.result.branchId;
              // for (let index = 0; index < responce.question.length; index++) {
              //   const element = array[index];

              // }
            });
        }
      }, (err) => {
        console.log(err);
      });

    this.param.paramMap
      .subscribe(params => {
        this.id = params.get('id');
        if (this.id != null) {
          this.isEdit = true;
        }
        else {
          this.isEdit = false;
        }
      });

    this.fullImagePath = '/assets/images/characterly-logo.png';
    this.edited = true;
    // this.questionModel.questionType = 0;
  }

  getContents() {
    this.spinner.show();
    this.contentService.getContents(0, 1000)
      .subscribe(res => {
        this.spinner.hide();
        this.contents = res.result;
      }, err=>{
        this.spinner.hide();
      });
  }


  buildHierarchy(domain) {

    if (domain.parentDomainId == null) {
        this.domainHierarchyList.push(domain);
    }
    else {
        this.domainHierarchyList = this.hierarchyLoop(domain, this.domainHierarchyList);
    }
}
domainLoop(domainHierarchyList){
    domainHierarchyList.forEach(element => {
        if (element.parentDomainId ==null ) {
            element.parentDomainId =element.name;
        }
        // else if(element.name != null){
        //     let obj=new Domain();
        //     obj.code=element.code;
        //     obj.domainCategory=element.domainCategory;
        //     obj.id=obj.id
        // }
    });
    return domainHierarchyList;

}

hierarchyLoop(newDomain, domainHierarchyList) {
    let flag: boolean;
    for (let i = 0; i < domainHierarchyList.length; i++) {
        const element = domainHierarchyList[i];
        if (flag == true)
            break;

        if (element.id == newDomain.parentDomainId) {
            newDomain.parentDomainId=element.name;
            domainHierarchyList.push(newDomain);
            //domainHierarchyList[i].childDomain.push(newDomain);
            flag = true;
            break;
        }
        // else if (element.childDomain != null) {
        //     domainHierarchyList[i].childDomain = this.hierarchyLoop(newDomain, element.childDomain);
        // }
    }
    return domainHierarchyList;
}

  addContent(){
    this.selectedContentName = this.sword;
  }

  searchWord(word) {
    if (word === "") {
      this.sword = "";
      this.selectedContentId = "";
      this.displayList = false;
    }
    else {
      this.displayList = true;
      this.searchResult = this.contentDatasource.filter(function (el: any) {
        return el.Name.toLowerCase().indexOf(word) > -1;
      });
    }

  }

  fillTextbox(contentList) {
    this.sword = "";
    this.selectedContentId = "";

    this.displayList = false;
    console.log(contentList);
    this.sword = contentList.Name;
    this.selectedContentId = contentList.id;

  }
  GetQuestion(value: string): void {

  }
  toNumber() {
    this.questionModel.questionType = +this.questionModel.questionType;
    alert(this.questionModel.questionType);
  }
  addQuestion(f, form: any) {
    debugger;
    if (form.descriptionInput && form.descriptionInput.length > 0) {
      this.flag = false;
      this.questionModel = new Questions();
      this.questionModel.description = form.descriptionInput;
      // tslint:disable-next-line:radix
      this.questionModel.questionType = parseInt(form.category);
      this.check = this.questionModel.questionType;
      this.questionModel.questionOptions = this.optionsList;
      // this.questionModel.AllowedMultiple=this.isMultiple
      this.questionModel.position = 0;
      this.questionModel.value = 1;
      this.questionsList.push(this.questionModel);
      this.optionsList = [];
      // this.arrayoptions = [];
      this.arrayoptions = [];
      this.description = "";
      this.questionEmpty=false;
    }else{
      this.questionEmpty=true;
    }
  }

  addoptions() {
    debugger;
    if(this.isImage && !this.fileUploadResponse){
      //this.error = "Please select an image first.";
      this.optionSubmitted = true;
      return;
    }

    if (this.option && this.option != "") {
      this.flag = true;
      this.arrayoptions.push(this.option);
      this.optionModel = new QuestionOptions();
      this.optionModel.description = this.option;
      if(this.fileUploadResponse && this.isImage){
        this.optionModel.fileAttachments = [this.fileUploadResponse.id];
        this.optionModel.imagePath=this.fileUploadResponse.path;
      }else{
        this.optionModel.fileAttachments = [];
        this.optionModel.imagePath='';
      }

      this.optionModel.position = 0;
      this.optionModel.isCorrect = false;
      this.optionsList.push(this.optionModel);
      this.option = '';
      this.fileUploadResponse = null; //Reset the images upload
      this.optionSubmitted = false;
      this.fileInput.nativeElement.value='';
    }else{
      this.optionSubmitted = true;
    }
  }

  singleOptionSelected(e, option, optionList){
    //if(e.target.checked){
      optionList.forEach(element => {
        if(element.description == option.description){
          element.isCorrect=true;
        }else{
          element.isCorrect=false;
        }
      });
      console.log(option.description);
    //}
  }

  multipleOptionSelected(e, option, optionList){
    let checked = (e.target.checked);
    //option.isCorrect=checked;

      optionList.forEach(element => {
        if(element.description == option.description){
          element.isCorrect=true;
          //break;
        }
      });
  }

  protected onSelected(item: CompleterItem) {

  }

  onTypeChange(value: any) {
    this.category = value;
    if (value === '0') {
      this.isOpen = true;
      this.isMultiple = false;
      this.isSingle = false;
      this.isImage = false;

    } else if (value === '1') {
      this.isOpen = false;
      this.isMultiple = true;
      this.isSingle = false;
      this.isImage = false;
    } else if (value === '2') {
      this.isOpen = false;
      this.isMultiple = false;
      this.isSingle = true;
      this.isImage = false;
    } else if (value === '3') {
      this.isOpen = true;
      this.isMultiple = false;
      this.isSingle = false;
      this.isTrueFalse= true;
      this.isImage = false;
    }  else if (value === '4') {
      this.isOpen = false;
      this.isMultiple = false;
      this.isSingle = false;
      this.isTrueFalse= false;
      this.isImage = true;
    }  else if (value === '5') {
      this.isOpen = false;
      this.isMultiple = true;
      this.isSingle = false;
      this.isTrueFalse= false;
      this.isImage = true;
    } else {
      alert('Select Answer Type First');
    }
  }
  onImageFileSelected(event){
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('file', file);
        //Multiple Option Category
        if(this.category == '4' || this.category == '5'){
          formData.append('category',  '6');
        }
         //********* Reset this for Question category */
        formData.append('entityId',  '');

        this.isOpen=true;

        this.fileService.UploadFile(formData).subscribe(response =>{

            // this.successMessage+=" Filed has been uploaded";
            if(response.success == true){
                this.fileUploadResponse = response.result;
                // this.newProfilePicture = response.result.id;
                // this.profilePicPath = response.result.path;
            }
            this.isOpen=false;
        },
        error => {
            this.error = "Upload attachment request is failed."
            this.isOpen=false;
        });
    }
}
  CreateEvaluation(form, f) {
    if(!form.valid){
      this.submitted=true;
      return;
    }

    var trees = jQuery("#content-multiselect").treeMultiselect();
    var firstTree = trees[0];
    let resultList:string[]=[];
    if(firstTree){
        for (let index = 0; index < firstTree.length; index++) {
          let element = firstTree[index]["value"];
          let op=element.split(": ",2);
          let kl=op[1].split("'",2)
          resultList.push(kl[1]);

      }
    }

    console.log(resultList);
    if (this.selectedContentId != "") {
      this.edited = true;
      this.loading = true;
      this.modeltopost = new EvaluationPost();
      this.modeltopost.name = this.title;
      this.modeltopost.BranchId = this.selectedBranchId;
      this.modeltopost.classroomId = '21d8b11e-953e-475b-bee6-08d51b12e05f';
      this.modeltopost.contentId = this.selectedContentId;
      this.modeltopost.questions = this.questionsList;

      console.log(this.modeltopost);
      if (this.isEdit == false) {
        this.EvaluationService.CreateEvaluation(this.modeltopost)
          .subscribe(result => {
            if (result.success === true) {
              this.loading = false;
              this.added = true;
              this.edited = false;
              this.title = "";
              this.selectedBranchId = "";
              this.selectedContentId = "";
              this.questionsList = [];
              this.sword = "";

              form.reset();
              this.showSuccessMessage("success", 'Evaluation Created Successfully');
            } else {
              this.error = 'Username or password is incorrect';
              this.loading = false;
            }
          });
      }
      else {
        this.modeltopost.id = this.id;
        this.EvaluationService.UpdateEvaluation(this.modeltopost)
          .subscribe(result => {
            if (result.success === true) {
              this.loading = false;
              this.added = true;
              this.edited = false;
              alert('Evaluation Updated Successfully');
              this.router.navigateByUrl("/dashboard/EvaluationManage");
            } else {
              this.error = 'Username or password is incorrect';
              this.loading = false;
            }
          });
      }

    } 
  }


  showSuccessMessage(type, message){
    if(type='success')
      this.successMessage = message;
    else
      this.error = message;

     setTimeout(() => {
         this.successMessage = '';
         this.error = '';
     }, 3000);
}

  RemoveQuestion(index){
    this.questionsList.splice(index,1);
  }
  RemoveOption(index){
    this.arrayoptions.splice(index,1);
    this.optionsList.splice(index,1);
  }
  autocompleListFormatter = (data: any)  => {
    let html = `<span>${data.title}</span>`;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
}
