import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import '../../assets/js/jquery.menu-aim.js';

declare var $:any;

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.css']
})
export class SidebarMenuComponent implements OnInit {
  @ViewChild('sidebar') el:ElementRef;
  constructor() {}

  ngOnInit() {
  }
  ngAfterViewInit() {

    function checkScrollbarPosition() {
  		var mq = checkMQ();

  		if( mq != 'mobile' ) {
  			var sidebarHeight = sidebar.outerHeight(),
  				windowHeight = $(window).height(),
  				mainContentHeight = mainContent.outerHeight(),
  				scrollTop = $(window).scrollTop();

  			( ( scrollTop + windowHeight > sidebarHeight ) && ( mainContentHeight - sidebarHeight != 0 ) ) ? sidebar.addClass('is-fixed').css('bottom', 0) : sidebar.removeClass('is-fixed').attr('style', '');
  		}
  		scrolling = false;
  	}

    function checkMQ() {
      if($(window).width() >= 1170)
        return 'desktop';
      else if($(window).width() < 1170 && $(window).width() > 768)
        return 'tablet';
      else
        return 'mobile';
    }

    var mainContent = $('.cd-main-content'),
    		header = $('.cd-main-header'),
    		sidebar = $('.cd-side-nav'),
    		sidebarTrigger = $('.cd-nav-trigger'),
    		topNavigation = $('.cd-top-nav'),
    		searchForm = $('.cd-search'),
    		accountInfo = $('.account');



    	//on window scrolling - fix sidebar nav
    	var scrolling = false;
    	checkScrollbarPosition();
    	$(window).on('scroll', function(){
    		if( !scrolling ) {
    			(!window.requestAnimationFrame) ? setTimeout(checkScrollbarPosition, 300) : window.requestAnimationFrame(checkScrollbarPosition);
    			scrolling = true;
    		}
    	});

    	//mobile only - open sidebar when user clicks the hamburger menu
    	sidebarTrigger.on('click', function(event){
    		event.preventDefault();
    		$([sidebar, sidebarTrigger]).toggleClass('nav-is-visible');
    	});

    	//click on item and show submenu
    	$('.has-children > a').on('click', function(event){

    		var mq = checkMQ(),
    			selectedItem = $(this);
    		if( mq == 'mobile' || mq == 'tablet' ) {
    			event.preventDefault();
    			if( selectedItem.parent('li').hasClass('selected')) {
    				selectedItem.parent('li').removeClass('selected');
    			} else {
    				sidebar.find('.has-children.selected').removeClass('selected');
    				accountInfo.removeClass('selected');
    				selectedItem.parent('li').addClass('selected');
    			}
    		}
    	});

    	//on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
    	sidebar.children('ul').menuAim({
            activate: function(row) {
            	$(row).addClass('hover');
            },
            deactivate: function(row) {
            	$(row).removeClass('hover');
            },
            exitMenu: function() {
            	sidebar.find('.hover').removeClass('hover');
            	return true;
            },
            submenuSelector: ".has-children",
        });

    // $(this.el.nativeElement).children('ul').menuAim({
    //       activate: function(row) {
    //         $(row).addClass('hover');
    //       },
    //       deactivate: function(row) {
    //         $(row).removeClass('hover');
    //       },
    //       exitMenu: function() {
    //         $(this.el.nativeElement).find('.hover').removeClass('hover');
    //         return true;
    //       },
    //       submenuSelector: ".has-children",
    //   });

    }

}
