﻿import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
 // styleUrls: ['./app.component.css']

})
export class AppComponent {
    title = 'app';
    username = '';
    ngOnInit() {
        // reset login status
        
        if (JSON.parse(localStorage.getItem('currentUser')) !=null && JSON.parse(localStorage.getItem('currentUser')) != '') {
            var currentuser = JSON.parse(localStorage.getItem('currentUser'));
            this.username = currentuser.username;
        }
      
    }


}