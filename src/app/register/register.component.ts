import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AccountService } from '../_services/index';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public registrationForm: FormGroup;
  constructor(
    private router:Router,
    private accountService: AccountService,
    private fb: FormBuilder) { 

    }
  public message: string;
  public loading: boolean;
  private formSubmitAttempt: boolean;
  public error: string;
  recoverPasswordForm: FormGroup;

  ngOnInit() {
    this.registrationForm = this.fb.group({
      Name: ['', [Validators.required]],
      Surname: ['', [Validators.required]],
      UserName: ['', [Validators.required]],
      EmailAddress: ['', [Validators.required, Validators.email]],
      Password: ['', [Validators.required, Validators.minLength(5)]],
      confirmPass: ['', Validators.required]
    },
      { validator: this.checkIfMatchingPasswords('Password', 'confirmPass') }
    );
  }
  save() {
    if (!this.registrationForm.valid) {
      this.formSubmitAttempt = true;
      return;
    }
    let user = this.registrationForm.value;
    this.loading = true;
    this.accountService.RegistrUser(user).subscribe(response => {

      this.showMessage('success', 'User hasbeen registered succesfully');
      this.loading = false;
      this.registrationForm.reset();
      this.router.navigate(['login']);
    },
      (error) => {
        console.log(error);
        if (error._body) {
          this.error = JSON.parse(error._body).error.message;
          this.showMessage('error',this.error );
        }
      });
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  showMessage(type, message) {
    if (type == 'success')
      this.message = message;
    else
      this.error = message;

    setTimeout(() => {
      this.message = '';
      this.error = '';
    }, 3000);
  }
}
