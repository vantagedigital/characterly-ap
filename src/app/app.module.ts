﻿import { RolesService } from './_services/roles.service';
import { LazyloadService } from './_services/lazyload.service';
import { StateService } from './_services/state.service';
import { CountryService } from './_services/country.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//import {NgAutoCompleteModule} from 'ng-auto-complete';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';
import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';
import { FullCalendarModule } from 'ng-fullcalendar';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2CompleterModule } from "ng2-completer";
import { AppOverview } from './overview/overview.component';
import { AppCheckComponent } from './overview/check.component';
import { FullLayoutComponent } from './FullLayout/FullLayout.component';


import { AppRolesComponent } from './roles/roles.component';
import { AuthenticationService, UserService, UserServices, BranchService, EvaluationService, ContentService, DomainService, SubjectService, PillarService, GradeService, ClassRoomService } from './_services/index';
import { LoginComponent } from './login/index';
import { LogoutComponent } from './logout/index';
import { AuthGuard } from './_guards/index';

import { DataTablesModule } from 'angular-datatables';
import { ChartsModule } from 'ng2-charts';
import { AppMaterialModule } from './app-material/app-material.module';
import { UserResponse } from './_models/user';
import { BranchResponse } from './_models/BranchModel';
import { EvaluationResponse } from './_models/EvaluationModel';
import { ContentResponse } from './_models/ContentModel';
import { ContentPost } from './_models/ContentModel';
import { UserPost } from './_models/user';
import { BranchPost } from './_models/BranchModel';
import { DomainResponse } from './_models/DomainModel';
import { DomainPost } from './_models/DomainModel';
import { EvaluationPost } from './_models/EvaluationModel';
import { PillarResponse } from './_models/PillarModel';
import { SubjectResponse } from './_models/SubjectModel';
import { GradeResponse } from './_models/GradeModel';
import { UserDetailComponent } from './users/userdetail.component';

import {MatDialogModule} from '@angular/material/dialog';
import { FileUploadModule } from 'ng2-file-upload';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarMenuComponent } from './sidebar-menu/sidebar-menu.component';

import { ScheduleService } from './_services/schedule.service';
import { RequestHelperService } from './_services/request-helper.service';

import { RequestService } from './_services/request.service';
import { MatInputModule, MatAutocompleteModule } from '@angular/material';
import { UserRoleService } from './_services/user-role.service';
import { FileAttachmentService } from './_services/file-attachment.service';
import { AccountService } from './_services/account.service';
import { NotificationsService } from './_services/notifications.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ScheduleDialogComponent } from './schedule/schedule-dialog/schedule-dialog.component';
import { BsModalModule } from 'ng2-bs3-modal';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { RegisterComponent } from './register/register.component';
import { RecoverCodeComponent } from './login/recover-code/recover-code.component';
import { RecoverPasswordComponent } from './login/recover-password/recover-password.component';
import { TreeModule } from 'angular-tree-component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import { MaterialModule } from '@angular2-material';
// import {  } from '@angular/material';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgSelectModule } from '@ng-select/ng-select';
import { TinyMceDirective } from './_directives/tinymce.directive';

import { ConfirmationDialogComponent, ConfirmationDialogsService } from './ui/confirmation-dialog/_index';
import { AppContentsEditComponent } from './contents/edit/edit.component';
import { AppContentsManageComponent } from './contents/manage/manage.component';
import { AppEvaluationsEditComponent } from './evaluations/edit/edit.component';
import { AppEvaluationsManageComponent } from './evaluations/manage/manage.component';
import { AppBranchesEditComponent } from './branches/edit/edit.component';
import { AppBranchesManageComponent } from './branches/manage/manage.component';
import { AppUsersEditComponent } from './users/edit/edit.component';
import { AppUsersManageComponent } from './users/manage/manage.component';
import { AppScheduleComponent } from './schedule/schedule.component';
import { AppRolesEditDialogComponent } from './roles/dialog/edit/edit-dialog.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab/tab.component';
import { DynamicTabsDirective } from './tabs/directive/dynamic-tabs.directive';
import { AppContentsEditFlashCardListComponent } from './contents/edit/flash-card/list/list.component';
import { AppContentsEditFlashCardEditComponent } from './contents/edit/flash-card/edit/edit.component';

const appRoutes: Routes = [
    { path: 'dashboard', redirectTo: '/Overview', pathMatch: 'full' },
    {
        path: 'dashboard',
        component: FullLayoutComponent,
        children: [
            { path: 'Overview', component: AppOverview },
            { path: 'ContentCreate', component: AppContentsEditComponent },
            { path: 'ContentManage', component: AppContentsManageComponent },
            { path: 'ContentEdit/:id', component: AppContentsEditComponent },

            { path: 'EvaluationCreate', component: AppEvaluationsEditComponent },
            { path: 'EvaluationManage', component: AppEvaluationsManageComponent },
            { path: 'EvaluationEdit/:id', component: AppEvaluationsEditComponent },

            { path: 'Schedule', component: AppScheduleComponent },

            { path: 'BranchCreate', component: AppBranchesEditComponent },
            { path: 'BranchManage', component: AppBranchesManageComponent },
            { path: 'BranchEdit/:id', component: AppBranchesEditComponent },

            { path: 'UserCreate', component: AppUsersEditComponent },
            { path: 'UserManage', component: AppUsersManageComponent },
            { path: 'UserEdit/:id', component: AppUsersEditComponent },

            { path: 'UserRoles', component: AppRolesComponent },
            { path: 'User/:id', redirectTo: '/SuperUser/:id' },
            { path: 'SuperUser/:id', component: UserDetailComponent }

        ],canActivate: [AuthGuard]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    
    {
        path: 'forgotpassword',
        component: ForgotPasswordComponent
    },
    { 
        path:'recovercode', 
        component:RecoverCodeComponent
    },
    { 
        path: 'recoverpassword', 
        component:RecoverPasswordComponent },
    //{
        //path: 'register',
        //component: RegisterComponent
    //},
    { path: 'logout', component: LogoutComponent, outlet: '' },
    { path: '', component: LoginComponent },
    { path: '**', component: PageNotFoundComponent },
];


@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        
        RouterModule.forRoot(appRoutes),
        DataTablesModule,
        ChartsModule,
        Ng2SmartTableModule,
        FormsModule,
        ReactiveFormsModule,
        AppMaterialModule,
        Ng2CompleterModule,
        FullCalendarModule,
        MatInputModule,
        ReactiveFormsModule,
        MatAutocompleteModule,
        HttpModule,
        // NgAutoCompleteModule,

        MatDialogModule,
        BsModalModule,
        MatSnackBarModule,
        FileUploadModule,
        TreeModule,
        NgxDatatableModule,
        NgxSpinnerModule,
        NgSelectModule
    ],

    declarations: [
        UserDetailComponent,
        LoginComponent,
        LogoutComponent,
        AppComponent,
        AppOverview,
        AppCheckComponent,
        AppContentsEditFlashCardEditComponent,
        AppContentsEditFlashCardListComponent,
        AppContentsEditComponent,
        AppContentsManageComponent,
        AppEvaluationsEditComponent,
        AppEvaluationsManageComponent,
        AppBranchesEditComponent,
        AppBranchesManageComponent,
        AppUsersEditComponent,
        AppUsersManageComponent,
        AppRolesComponent,
        HeaderComponent,
        FooterComponent,
        SidebarMenuComponent,
        FullLayoutComponent,
        AppScheduleComponent,
        PageNotFoundComponent,
        ScheduleDialogComponent,
        ForgotPasswordComponent,
        RegisterComponent,
        RecoverCodeComponent,
        RecoverPasswordComponent,
        TinyMceDirective,
        ConfirmationDialogComponent,
        AppRolesEditDialogComponent,
        TabsComponent, 
        TabComponent, 
        DynamicTabsDirective, 
    ],

    providers: [
        EvaluationPost,
        DomainResponse,
        DomainPost,
        DomainService,
        ContentPost,
        ContentResponse,
        AuthenticationService,
        UserService,
        UserServices,
        UserResponse,
        BranchService,
        ClassRoomService,
        BranchResponse,
        EvaluationService,
        EvaluationResponse,
        AuthGuard,
        UserPost,
        BranchPost,
        PillarService,
        PillarResponse,
        SubjectService,
        SubjectResponse,
        GradeService,
        GradeResponse,
        ContentService,
        ScheduleService,
        RequestHelperService,
        RequestService,
        CountryService,
        StateService,
        LazyloadService,
        UserRoleService,
        RolesService,
        FileAttachmentService,
        AccountService,
        NotificationsService,
        CookieService,
        ConfirmationDialogsService,
    ],
    bootstrap: [AppComponent],
    entryComponents:[ConfirmationDialogComponent, AppRolesEditDialogComponent, TabComponent]
    
})
export class AppModule { }
