﻿import { Component, OnInit } from '@angular/core';
import {NotificationsService} from '../_services/notifications.service';
declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private notificationsService: NotificationsService ) { }

  public userName: string;
  public notificationsCount:number;
  ngOnInit() {

      var currentuser = JSON.parse(localStorage.getItem('currentUser'));
      this.userName = currentuser.username;
      this.getNotifications();

  }

  getNotifications(){
    this.notificationsService.GetAllNotifications().subscribe(response =>
			{
				this.notificationsCount = response.result.length;
			});
  }

  ngAfterViewInit(){

    var header = $('.cd-main-header'),
		sidebar = $('.cd-side-nav'),
		topNavigation = $('.cd-top-nav'),
		searchForm = $('.cd-search'),
		accountInfo = $('.account');

    function moveNavigation(){
  		var mq = checkMQ();
      if ( mq == 'mobile' && topNavigation.parents('.cd-side-nav').length == 0 ) {
      	detachElements();
  			topNavigation.appendTo(sidebar);
  			searchForm.removeClass('is-hidden').prependTo(sidebar);
  		}
  		else if ( ( mq == 'tablet' || mq == 'desktop') &&  topNavigation.parents('.cd-side-nav').length > 0 ) {
  			detachElements();
  			searchForm.insertAfter(header.find('.cd-logo'));
  			topNavigation.appendTo(header.find('.cd-nav'));
  		}
  		checkSelected(mq);
  		resizing = false;
  	}

    function detachElements() {
  		topNavigation.detach();
  		searchForm.detach();
  	}

    //on resize, move search and top nav position according to window width
  	var resizing = false;
  	moveNavigation();
  	$(window).on('resize', function(){
  		if( !resizing ) {
  			(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
  			resizing = true;
  		}
  	});

    //click on account and show submenu - desktop version only

    accountInfo.children('a').on('click', function(event){
      var mq = checkMQ(),
  			selectedItem = $(this);
  		if( mq == 'desktop') {
        if(accountInfo.hasClass( "selected" )){
          $('.account').attr('class', 'has-children account')
          event.preventDefault();
        }
        else{
          $('.account').attr('class', 'has-children account selected')
          event.preventDefault();
        }
  			// accountInfo.toggleClass('selected');
  			sidebar.find('.has-children.selected').removeClass('selected');
  		}
  	});

  	$(document).on('click', function(event){
  		if( !$(event.target).is('.has-children a') ) {
        sidebar.find('.has-children.selected').removeClass('selected');
  			accountInfo.removeClass('selected');
  		}
  	});

    function checkMQ() {
  		if($(window).width() >= 1170)
  			return 'desktop';
  		else if($(window).width() < 1170 && $(window).width() > 768)
  			return 'tablet';
  		else
  			return 'mobile';
  	}

    function checkSelected(mq) {
  		//on desktop, remove selected class from items selected on mobile/tablet version
  		if( mq == 'desktop' ) $('.has-children.selected').removeClass('selected');
  	}
  }

}
