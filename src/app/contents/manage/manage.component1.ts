import { CreateNewAutocompleteGroup } from 'ng-auto-complete';
import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { TinyMceDirective } from '../../_directives/tinymce.directive'

import { ContentService } from '../../_services/index';
import { ContentResponse } from '../../_models/ContentModel';
import { Content } from '../../_models/ContentModel';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { ContentType } from '../../_models/ContentModel';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';

const DEFAULT_PAGE_SIZE = 5;

@Component({
  selector: 'app-contents-manage',
  templateUrl: 'manage.component.html',
  styleUrls: ['manage.component.css'],
})


export class AppContentsManageComponent implements OnInit, OnDestroy {
  
  successMessage='';
  ContentFullResponse: ContentResponse;

  contents: Content[]=[];
  selectedContents: Content[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
	filter:string;
	sort:string;
	dir: string;

  loading = false;
  error = '';

  loadingIndicator = true;
  reorderable = true;

  searchControl = new FormControl();
  componentDestroyed = new Subject(); // Component Destroy

  ContentType: typeof ContentType = ContentType;
  options = Object.keys(ContentType);

  
  constructor(
    private viewContainer: ViewContainerRef,
    private router: Router,
    private ContentService: ContentService,
    private ContentResponse: ContentResponse,
    private confirmationDialogsService: ConfirmationDialogsService) { }


  ngOnInit() {
    this.getContents();

    this.searchControl.valueChanges
		.debounceTime(300) 
      .distinctUntilChanged()  
      .takeUntil(this.componentDestroyed)
      .subscribe( filter => { 
        this.filter = filter ? filter : '';
        this.getContents();
      });
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  getContents(params=null) {
    this.loadingIndicator = true;
    let query = {
      skipCount: this.pageNumber * this.pageSize,
      maxResultCount: this.pageSize,
      filter: this.filter,
      ...params
    }

    this.ContentService.getSearchContents(query).subscribe(
      (data) => {
        this.loadingIndicator = false;
        this.ContentFullResponse = data;
        this.contents = data.result;
        console.log(data);
      }, (err) => {
        this.loadingIndicator = false;
        console.log(err);
      });    
  }

  deleteSelectedContents() {
    if (this.selectedContents.length > 0) {

      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Contents?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {

          if (result) {
            for (let index = 0; index < this.selectedContents.length; index++) {
              let id = this.selectedContents[index].id;
              this.ContentService.DeleteContent(id)
                .subscribe(responce => {
                  this.selectedContents.splice(index, 1);
                  this.getContents();
                }, (err) => {
                  console.log(err);
                });
            }
          }
        });
    } else {
      this.showMessage('error', "Please select the Content to delete");

    }

  }

  deleteContent(contentId) {

    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Content?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {
        if (result) {

          this.ContentService.DeleteContent(contentId)
            .subscribe(responce => {
              this.getContents();
            }, (err) => {
              console.log(err);
            });
        }
      });
  }

	onSelect({ selected }) {
    this.selectedContents.splice(0, this.selectedContents.length);
    this.selectedContents.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
    this.getContents();
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getContents();
  }

  updateFilter(term: string): void {
    this.filter = term;
    this.getContents();
  }


  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }


  convertArrayNameToString(arrObj: any[]) {
    let nameArr = arrObj.map( obj => { return obj.name } );
    return nameArr.join(",");
  }

  getContentType(contentType: string) {
    return this.options.findIndex( o=> contentType = this.ContentType[o]);
  }

  min(x, y) {
    return Math.min(x, y);
  }
}
