import { CreateNewAutocompleteGroup } from 'ng-auto-complete';
import { Component, OnInit, ViewContainerRef, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { TinyMceDirective } from '../../_directives/tinymce.directive'

import { ContentService } from '../../_services/index';
import { ContentResponse } from '../../_models/ContentModel';
import { Content } from '../../_models/ContentModel';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { ContentType } from '../../_models/ContentModel';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { NgxSpinnerService } from 'ngx-spinner';

const DEFAULT_PAGE_SIZE = 5;
const MAX_COUNT = 1000;

@Component({
  selector: 'app-contents-manage',
  templateUrl: 'manage.component.html',
  styleUrls: ['manage.component.css'],
})


export class AppContentsManageComponent implements OnInit, OnDestroy {
  
  successMessage='';
  error = '';

  contents: Content[]=[];
  filteredContents: Content[]=[];
  selectedContents: Content[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
  loadingIndicator = false;
  reorderable = true;

  componentDestroyed = new Subject(); // Component Destroy

  ContentType: typeof ContentType = ContentType;
  options = Object.keys(ContentType);

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput') search: ElementRef;
  
  constructor(
    private spinner: NgxSpinnerService,
    private viewContainer: ViewContainerRef,
    private router: Router,
    private ContentService: ContentService,
    private ContentResponse: ContentResponse,
    private confirmationDialogsService: ConfirmationDialogsService) { }


  ngOnInit() {
    this.getContents();
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  getContents() {
    this.spinner.show();
    this.ContentService.getContents(0, MAX_COUNT).subscribe(
      (data) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
        this.contents = data.result;
        this.total = this.contents.length;
        this.updateFilter(this.search.nativeElement.value);        
      }, (err) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
      });    
  }

  deleteSelectedContents() {
    if (this.selectedContents.length > 0) {

      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Contents?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {

          if (result) {
            for (let index = 0; index < this.selectedContents.length; index++) {
              let id = this.selectedContents[index].id;
              this.ContentService.DeleteContent(id)
                .subscribe(responce => {
                  let index = this.contents.findIndex(x=>x.id==id);
                  if (index != -1) {
                    this.contents.splice(index, 1);
                    this.updateFilter(this.search.nativeElement.value);
                  }
                  //this.getContents();
                }, (err) => {
                  console.log(err);
                });
            }
          }
        });
    } else {
      this.showMessage('error', "Please select the Content to delete");

    }

  }

  deleteContent(contentId) {

    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected Content?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {
        if (result) {
          this.spinner.show();
          this.ContentService.DeleteContent(contentId)
            .subscribe(responce => {
              this.spinner.hide();
              let index = this.contents.findIndex(x=>x.id==contentId);
              if (index != -1) {
                this.contents.splice(index, 1);
                this.updateFilter(this.search.nativeElement.value);
              }
              //this.getContents();
            }, (err) => {
              this.spinner.hide();
              console.log(err);
            });
        }
      });
  }

	onSelect({ selected }) {
    this.selectedContents.splice(0, this.selectedContents.length);
    this.selectedContents.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getContents();
  }

  updateFilter(term: string): void {
    const val = term.toLowerCase();

    const filteredContents = this.contents.filter(function (cat) {
        return Object.keys(cat).some(key => {
            return (cat[key] ? cat[key] : '')
                .toString()
                .toLowerCase()
                .indexOf(val) !== -1 || !val;
        })
    });

    this.filteredContents = filteredContents;
    if (this.table) {
        this.table.offset = 0;
    }
  }


  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }


  convertArrayNameToString(arrObj: any[]) {
    let nameArr = arrObj.map( obj => { return obj.name } );
    return nameArr.join(",");
  }

  getContentType(contentType: string) {
    return this.options.findIndex( o=> contentType = this.ContentType[o]);
  }

  min(x, y) {
    return Math.min(x, y);
  }
}
