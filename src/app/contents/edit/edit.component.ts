﻿import {
  Component,
  ViewChild,
  ElementRef,
  Pipe,
  PipeTransform,
  OnInit,
  EventEmitter,
  Output,
  Input,
  AfterViewChecked,
  SecurityContext,
  ViewContainerRef
} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";

import { FileUploader } from "ng2-file-upload";
import {
  ITreeOptions,
  TreeModel,
  TreeNode,
  ITreeState
} from "angular-tree-component";

import { UserServices } from "../../_services/index";
import { UserPost } from "../../_models/user";
import { BranchService } from "../../_services/index";
import { BranchResponse } from "../../_models/BranchModel";
import { BranchResult } from "../../_models/BranchModel";
import { Branch } from "../../_models/BranchModel";
import { BranchPost } from "../../_models/BranchModel";
import { RequestService } from "../../_services/request.service";
import { DomainService } from "../../_services/index";
import { DomainResponse, DomainChildList } from "../../_models/DomainModel";
import { DomainResult } from "../../_models/DomainModel";
import { Domain } from "../../_models/DomainModel";
import { DomainOptions } from "../../_models/DomainModel";

import { PillarService } from "../../_services/index";
import { PillarResponse } from "../../_models/PillarModel";
import { Pillar } from "../../_models/PillarModel";
import { PillarOptions } from "../../_models/PillarModel";

import { SubjectService } from "../../_services/index";
import { SubjectResponse } from "../../_models/SubjectModel";
import { Subject } from "../../_models/SubjectModel";
import { SubjectOptions } from "../../_models/SubjectModel";

import { GradeService } from "../../_services/index";
import { GradeResponse } from "../../_models/GradeModel";
import { GradeResult } from "../../_models/GradeModel";
import { Grade } from "../../_models/GradeModel";
import { GradeOptions } from "../../_models/GradeModel";

import { ContentService } from "../../_services/index";
import { ContentResponse } from "../../_models/ContentModel";
import { Content } from "../../_models/ContentModel";
import { ContentPost } from "../../_models/ContentModel";
import { ContentType } from "../../_models/ContentModel";
import "../../../assets/js/jquery.tree-multiselect.min.js";
import { CountryService } from "../../_services/country.service";
import { GeneralList } from "../../_models/general-code";
import { FileAttachmentService } from "../../_services/file-attachment.service";
import { UploadFileAttachment } from "../../_models/file-attachment";
import { ScheduleService } from "../../_services/schedule.service";
import { ConfirmationDialogsService } from "../../ui/confirmation-dialog/_index";
import { TabsComponent } from "../../tabs/tabs.component";
import { NgxSpinnerService } from "ngx-spinner";

declare var Taggle: any;
declare var jQuery: any;
declare var tinyMCE: any;
declare var $: any;
@Component({
  moduleId: module.id,
  selector: 'app-contents-edit',
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class AppContentsEditComponent implements OnInit {
  submitted = false;
  successMessage = "";
  public arrayOfStrings = ["this", "is", "list", "of", "string", "element"];
  public isRequired: boolean = true;
  model: any = {};
  options: string[];
  ContentTypeValue: ContentType;
  editor;
  //onEditorKeyup = new EventEmitter<any>();
  ContentType: typeof ContentType = ContentType;
  contentImage = "";
  contentImageId = "";

  titleAlert: string = "This field is required";
  selectedBranchId: string = "-1";
  selectedDomainIds = [];
  selectedPillarIds: string[];
  selectedSubjectIds: string[];
  selectedGradeIds: string[];
  selectedLanguage: string;
  currentSelectedTab: number = 0;
  contentPageImagePath = "";
  AllDomains: string[];

  DomainsFullResponse: DomainResponse;
  AllDomainsWithCount: DomainResult;
  AllDomainsOnly: Domain[];
  AllDomainsWithCheckBoxes = [];

  BranchesFullResponse: BranchResponse;
  AllBranchesWithCount: BranchResult;
  AllBranchesOnly: Branch[];

  GradesFullResponse: GradeResponse;
  AllGradesWithCount: GradeResult;
  AllGradesOnly: Grade[];
  AllGradesWithCheckBoxes = [];

  PillarsFullResponse: PillarResponse;
  AllPillarsOnly: Pillar[];
  AllPillarsWithCheckBoxes = [];

  SubjectsFullResponse: SubjectResponse;
  AllSubjectsOnly: Subject[];
  AllSubjectsWithCheckBoxes = [];

  modeltopost: ContentPost;
  ContentFullResponse: ContentResponse;
  AllContentesOnly: Content[];

  public edited = false;
  public added = false;
  loading = false;
  error = "";
  errorFlag = false;
  fullImagePath: string;
  EditorContent: string;
  form: FormGroup;

  myFile: File;
  isEdit: boolean;
  id: string;
  title: string;
  description: string;
  extendedText: string;
  contentTags: string;
  selectedContentType: string = "-1";
  contentText: string = "";

  language: string = "-1";
  inputFile: any;
  domains: string[] = [];
  flag: boolean;
  tinymce: any;
  taggles;
  countryList: GeneralList[] = [];
  selectedCountry: number = -1;
  onloadFlag: boolean;
  public domainHierarchyList: DomainChildList[] = [];
  domainLoopList: Domain[] = [];
  ddd: any;
  public uploader: FileUploader = new FileUploader({
    url: this.requestService.baseURL + "services/app/FileAttachment/Upload"
  });
  nodes: any = [];
  pillarsFound: boolean = false;
  gradesFound: boolean = false;
  subjectFound: boolean = false;
  ourState: ITreeState;
  private multiSelectTree: any;
  private fileUploadResponse: any;
  optionsTree: ITreeOptions = {
  };

  cards: any=[];

  @ViewChild(TabsComponent) tabsComponent;
  @ViewChild('cardEdit') editFlashCardTemplate;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private userService: UserServices,
    private BranchService: BranchService,
    private ContentService: ContentService,
    private BrancesResponse: BranchResponse,
    private DomainService: DomainService,
    private DomainResponse: DomainResponse,
    private SubjectService: SubjectService,
    private GradeService: GradeService,
    private PillarService: PillarService,
    private formBuilder: FormBuilder,
    private param: ActivatedRoute,
    private country: CountryService,
    private fileService: FileAttachmentService,
    private scheduleService: ScheduleService,
    private requestService: RequestService,
    private sanitizer: DomSanitizer,
    private confirmationDialogsService: ConfirmationDialogsService,
    private viewContainer: ViewContainerRef
  ) {
    this.isEdit = false;
    this.flag = false;
    this.onloadFlag = false;
  }

  @ViewChild("contentText1") tiny: ElementRef;

  @Output() onEditorKeyup = new EventEmitter<any>();

  clearMessages() {
    this.successMessage = "";
  }

  deleteFileAttachment() {
    this.confirmationDialogsService
      .confirm(
        "Confirmation",
        "Do you really want to delete the attachment?",
        this.viewContainer,
        "  OK  ",
        "Cancel"
      )
      .subscribe(result => {
        if (result) {
          this.fileService.Delete(this.contentImageId).subscribe(res => {
            this.contentImage = null;
          });
        }
      });
  }
  
  onContentFileSelected(event) {
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      let file: File = fileList[0];
      let formData: FormData = new FormData();
      formData.append("file", file);
      formData.append("category", "1");
      formData.append("entityId", "");

      this.fileService.UploadFile(formData).subscribe(
        response => {
          this.successMessage += " Filed has been uploaded";
          if (response.success == true) {
            this.fileUploadResponse = response.result;
            this.contentImage = this.fileUploadResponse.path;
            this.contentImageId = this.fileUploadResponse.id;
          }
        },
        error => {
          this.error = "Upload attachment request is failed.";
        }
      );
    }
  }

  ngOnInit() {
    this.clearMessages();
    this.contentText = "";
    this.selectedContentType = "-1";
    this.selectedBranchId = "-1";
    this.language = "-1";
    this.selectedCountry = -1;

    setTimeout(() => {
      if (this.flag == false) {
        new Taggle("content-tags");
      }
      this.multiSelectTree = $("#content-multiselect").treeMultiselect({
        allowBatchSelection: true,
        startCollapsed: true,

        searchable: true,
        sortable: false
      });
      //this.multiSelectTree = tree[0];
    }, 4000);

    this.fullImagePath = "/assets/images/characterly-logo.png";
    this.edited = true;
    var options = Object.keys(ContentType);
    this.options = options.slice(options.length / 2);
    let dataArray = [
      { id: 1, name: "USA" },
      { id: 1, name: "UK" },
      { id: 1, name: "Italy" }
    ];
    let minCharsForSearch = 1;
    let placeholder = "search.."; //min amount of chars before autocomplete mayches results
    let responseApiKey = "id"; //a reference to the key for example api response [{"id":1,"name":"USA"}, {..}]
    let responseApiText = "name"; //a reference to the text for example api response [{"id":1,"name":"USA"}, {..}]

    this.param.paramMap.subscribe(params => {
      this.id = params.get("id");
      if (this.id != null) {
        this.isEdit = true;
      } else {
        this.isEdit = false;
      }
    });

    this.BranchService.GetBranchesList().subscribe(
      data => {
        this.BranchesFullResponse = data;
        this.AllBranchesWithCount = data.result;
        this.AllBranchesOnly = data.result.items;
      },
      err => {
        console.log(err);
      }
    );

    this.getGradesList();
    this.getPillarsList();
    this.getSubjectList();

    this.DomainService.GetDomainsList().subscribe(
      data => {
        this.edited = false;
        this.DomainsFullResponse = data;
        this.AllDomainsWithCount = data.result;
        this.AllDomainsOnly = data.result;

        let temp = [];

        let parents = data.result.filter(x => x.parentDomainId == null);
        let childs = data.result.filter(x => x.parentDomainId != null);

        for (let i = 0; i < parents.length; i++) {
          let node: any = {};
          node.id = this.AllDomainsOnly[i].id;
          node.name = this.AllDomainsOnly[i].name;
          node.parentDomainId = this.AllDomainsOnly[i].parentDomainId;
          let myChilds = childs.filter(x => x.parentDomainId == node.id);

          node.children = myChilds;
          temp.push({ id: node.id, name: node.name, children: node.children });
        }
        this.nodes = temp;

        var a = false;
        for (let i = 0; i < this.AllDomainsOnly.length; i++) {
          let dcl = new DomainChildList();
          dcl.code = this.AllDomainsOnly[i].code;
          dcl.domainCategory = this.AllDomainsOnly[i].domainCategory;
          dcl.id = this.AllDomainsOnly[i].id;
          dcl.name = this.AllDomainsOnly[i].name;
          dcl.parentDomainId = this.AllDomainsOnly[i].parentDomainId;
          dcl.checked = false;
          dcl.childDomain = [];
          this.buildHierarchy(dcl);

          const objDomainoption = new DomainOptions();
          objDomainoption.name = this.AllDomainsOnly[i].name;
          objDomainoption.value = this.AllDomainsOnly[i].id;
          objDomainoption.checked = false;
          this.AllDomainsWithCheckBoxes.push(objDomainoption);
          a = true;
        }
        this.domainHierarchyList = this.domainLoop(this.domainHierarchyList);
      },
      err => {
        console.log(err);
      }
    );
    this.country.getAll().subscribe(response => {
      for (let index = 0; index < response.result.length; index++) {
        const element = response.result[index];
        let country = new GeneralList();
        country.id = element.id;
        country.Name = element.name;
        this.countryList.push(country);
      }
    });
  }

  selectNode(node: TreeNode): void {
    node.data.checked = true;

    /*if (father != null) {}*/
    node.data.children.forEach((nodePai: any) => {
      nodePai.checked = true; 
      nodePai.checkbox = true; 
    });
  }
  onEvent($event) {
    if ($event.eventName == "select") {
      let clickedIctem = $event.treeModel.focusedNodeId;
      let selectedItems = Object.keys($event.treeModel.selectedLeafNodeIds);
      var childItemClicked = selectedItems.find(x => x == clickedIctem);
      let SelectedIds = [];
      //if child item clicked then only pick clicked item otherwise pick selected items and focused item
      if (childItemClicked) {
        SelectedIds.push(childItemClicked);
      } else {
        SelectedIds.push(clickedIctem); // Get Parent
        //Get Children
        selectedItems.forEach(element => {
          SelectedIds.push(element);
        });
      }
    }
  }

  getGradesList(branchId = "") {
    let gradeListSubs;
    if (branchId === "") {
      gradeListSubs = this.GradeService.GetGradesList();
    } else {
      gradeListSubs = this.GradeService.GetGradesListByBranchId(branchId);
    }
    gradeListSubs.subscribe(
      data => {
        this.edited = false;
        this.GradesFullResponse = data;
        if (branchId === "") {
          this.AllGradesWithCount = data.result;
          this.AllGradesOnly = data.result.items;
        } else {
          this.AllGradesOnly = data.result;
        }

        if (this.AllGradesOnly.length > 0) {
          for (var i = 0; i < this.AllGradesOnly.length; i++) {
            let objGradeOption = new GradeOptions();
            objGradeOption.name = this.AllGradesOnly[i].name;
            objGradeOption.value = this.AllGradesOnly[i].id;
            objGradeOption.checked = false;
            this.AllGradesWithCheckBoxes.push(objGradeOption);
          }

          this.gradesFound = true;
        } else {
          this.AllGradesWithCheckBoxes = [];
          this.gradesFound = false;
        }
      },
      err => {
        console.log(err);
      }
    );
  }
  getPillarsList(branchId: string = "") {
    let pillarListSubs;
    if (branchId === "") {
      pillarListSubs = this.PillarService.GetPillarsList();
    } else {
      pillarListSubs = this.PillarService.GetPillarsListByBranchId(branchId);
    }
    pillarListSubs.subscribe(
      data => {
        this.edited = false;
        this.PillarsFullResponse = data;
        this.AllPillarsOnly = data.result;

        if (this.AllPillarsOnly.length > 0) {
          for (var i = 0; i < this.AllPillarsOnly.length; i++) {
            let objPillarOption = new PillarOptions();
            objPillarOption.name = this.AllPillarsOnly[i].name;
            objPillarOption.value = this.AllPillarsOnly[i].id;
            objPillarOption.checked = false;
            this.AllPillarsWithCheckBoxes.push(objPillarOption);
          }
          this.pillarsFound = true;
        } else {
          this.AllPillarsWithCheckBoxes = [];
          this.pillarsFound = false;
        }
      },
      err => {
        console.log(err);
      }
    );
  }
  getSubjectList(branchId = "") {
    let subjectistSubs;
    if (branchId === "") {
      subjectistSubs = this.SubjectService.GetSubjectsList();
    } else {
      subjectistSubs = this.SubjectService.GetSubjectListByBranchId(branchId);
    }
    subjectistSubs.subscribe(
      data => {
        this.edited = false;
        this.SubjectsFullResponse = data;
        this.AllSubjectsOnly = data.result;
        if (this.AllSubjectsOnly.length > 0) {
          for (var i = 0; i < this.AllSubjectsOnly.length; i++) {
            let objSubjectOption = new SubjectOptions();
            objSubjectOption.name = this.AllSubjectsOnly[i].name;
            objSubjectOption.value = this.AllSubjectsOnly[i].id;
            objSubjectOption.checked = false;
            this.AllSubjectsWithCheckBoxes.push(objSubjectOption);
          }
          this.subjectFound = true;
        } else {
          this.AllSubjectsWithCheckBoxes = [];
          this.subjectFound = false;
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  onChangeBranch(value) {
    this.selectedBranchId = value;
    this.getGradesList(value);
    this.getPillarsList(value);
    this.getSubjectList(value);
  }

  buildHierarchy(domain) {
    if (domain.parentDomainId == null) {
      this.domainHierarchyList.push(domain);
    } else {
      this.domainHierarchyList = this.hierarchyLoop(
        domain,
        this.domainHierarchyList
      );
    }
  }
  domainLoop(domainHierarchyList) {
    domainHierarchyList.forEach(element => {
      if (element.parentDomainId == null) {
        element.parentDomainId = element.name;
      }
    });
    return domainHierarchyList;
  }

  hierarchyLoop(newDomain, domainHierarchyList) {
    let flag: boolean;
    for (let i = 0; i < domainHierarchyList.length; i++) {
      const element = domainHierarchyList[i];
      if (flag == true) break;

      if (element.id == newDomain.parentDomainId) {
        newDomain.parentDomainId = element.name;
        domainHierarchyList.push(newDomain);
        flag = true;
        break;
      }
    }
    return domainHierarchyList;
  }
  onEdit() {
    if (this.flag == false && this.id != null) {
      this.ContentService.GetContent(this.id).subscribe(response => {
        this.flag = true;
        let result = response.result;
        this.selectedBranchId = result.branchId;
        this.ContentTypeValue = result.contentType;
        this.selectedContentType = ContentType[result.contentType];
        this.contentText = result.contentText;
        this.description = result.description;
        this.selectedCountry = result.countryId;
        this.title = result.title;
        this.extendedText = result.footprint;
        this.taggles = result.contentTags;


        this.fileService.GetByEntityId(this.id).subscribe(res => {
          if (res.result.length > 0) {
            this.contentImageId = res.result[0].id;
            this.contentImage = res.result[0].path;
          }
        });

        new Taggle("content-tags", {
          tags: result.contentTags.map(item => {
            return item.name;
          })
        });
        for (let index = 0; index < result.contentTags.length; index++) {
          if (index != 0) {
            this.contentTags =
              this.contentTags + "," + result.contentTags[index];
          } else {
            this.contentTags = result.contentTags[index];
          }
        }
        this.language = result.language;
        let options = $("#content-multiselect").find("option");
        for (var i = 0; i < options.length; i++) {
          options[i].selected = true;
          let element = $(options[i]).attr("value");
          let op = element.split(": ", 2);
          let kl = op[1].split("'", 2);
          if (result.domains.find(i => i.id == kl[1])) {
            $(options[i]).attr("selected", "selected");
          }
        }

        for (let index = 0; index < result.pillars.length; index++) {
          for (
            let index2 = 0;
            index2 < this.AllPillarsWithCheckBoxes.length;
            index2++
          ) {
            if (
              this.AllPillarsWithCheckBoxes[index2].value ==
              result.pillars[index].id
            ) {
              this.AllPillarsWithCheckBoxes[index2].checked = true;
            }
          }
        }
        for (let index = 0; index < result.subjects.length; index++) {
          for (
            let index2 = 0;
            index2 < this.AllSubjectsWithCheckBoxes.length;
            index2++
          ) {
            if (
              this.AllSubjectsWithCheckBoxes[index2].value ==
              result.subjects[index].id
            ) {
              this.AllSubjectsWithCheckBoxes[index2].checked = true;
            }
          }
        }
        for (let index = 0; index < result.grades.length; index++) {
          for (
            let index2 = 0;
            index2 < this.AllGradesWithCheckBoxes.length;
            index2++
          ) {
            if (
              this.AllGradesWithCheckBoxes[index2].value ==
              result.grades[index].id
            ) {
              this.AllGradesWithCheckBoxes[index2].checked = true;
            }
          }
        }
      });
      this.flag = true;
    }
  }

  parseValue(value: string) {
    this.ContentTypeValue = ContentType[value];
  }

  getTextContent(event) {
    this.contentText = event;
  }



  resetForm(f) {
    if (f.valid) {
      f.reset();
      this.contentText = "";
     
      new Taggle("content-tags", {
        tags: []
      });

      this.contentText = "";
      this.contentImage = null;
    }
  }

  CreateContent(f, obj) {
    if (
      !f.valid ||
      this.selectedContentType == "-1" ||
      this.selectedBranchId == "-1" ||
      this.language == "-1" ||
      this.selectedCountry == -1
    ) {
      this.submitted = true;
      return;
    }
    this.clearMessages();
	
    var domainTreeRoots = $(".tree-multiselect .selected").find(".item");
    let resultList: string[] = [];
	
    for (let index = 0; index < domainTreeRoots.length; index++) {
      let element = domainTreeRoots[index].getAttribute("data-value");
      let op = element.split(": ", 2);
      let kl = op[1].split("'", 2);
      resultList.push(kl[1]);
    }

    this.edited = true;
    this.loading = true;
    this.modeltopost = new ContentPost();
    let contentDate = obj.contentDate;
    this.modeltopost.branchId = this.selectedBranchId;
    this.modeltopost.language = this.language;
    this.modeltopost.countryId = +this.selectedCountry;
    this.modeltopost.status = 0;
    this.modeltopost.title = this.title;
    this.modeltopost.contentText = this.sanitizer.sanitize(
      SecurityContext.HTML,
      this.contentText
    );
    

    this.modeltopost.description = this.description;
    this.modeltopost.footPrint = this.extendedText;
    this.modeltopost.contentType = this.ContentTypeValue;
    var tagMultiSelect = $("input[name='taggles[]']")
      .map(function() {
        return $(this)
          .val()
          .toString();
      })
      .get()
      .toString();

    let tags = tagMultiSelect.split(",");

    this.modeltopost.contentTags = tags;
    this.modeltopost.pillarIds = [];
    this.modeltopost.gradeIds = [];
    this.modeltopost.subjectIds = [];
    this.modeltopost.domainIds = resultList;

    for (var i = 0; i < this.AllPillarsWithCheckBoxes.length; i++) {
      if (this.AllPillarsWithCheckBoxes[i].checked == true)
        this.modeltopost.pillarIds.push(this.AllPillarsWithCheckBoxes[i].value);
    }

    for (var i = 0; i < this.AllSubjectsWithCheckBoxes.length; i++) {
      if (this.AllSubjectsWithCheckBoxes[i].checked == true)
        this.modeltopost.subjectIds.push(
          this.AllSubjectsWithCheckBoxes[i].value
        );
    }

    for (var i = 0; i < this.AllGradesWithCheckBoxes.length; i++) {
      if (this.AllGradesWithCheckBoxes[i].checked == true)
        this.modeltopost.gradeIds.push(this.AllGradesWithCheckBoxes[i].value);
    }

    if (this.isEdit != true) {
      this.ContentService.CreateContent(this.modeltopost).subscribe(
        result => {
          if (result.success === true) {
            this.successMessage += "Content successfully added.";
            if (contentDate != null) {
              let data = {
                BranchId: result.result.branchId,
                ContentId: result.result.id, //"5e121d9a-0fc0-4ca3-2490-08d535bb9c12",
                ClassroomIds: [],
                Language: result.result.language,
                CountryId: result.result.countryId,
                Status: 2,
                Title: result.result.title,
                Description: result.result.description,
                StartDate: contentDate,
                EndDate: contentDate
              };
              this.scheduleService.createSchedule(data).subscribe(
                response => {
                  this.successMessage += "  Schedule successfully added.";
                  this.loading = false;
                  obj = "";
                },
                error => {
                  this.loading = false;
                  this.errorFlag = true;
                  obj = "";
                  this.error = "Schedule cannot be added.";
                }
              );
            }

            this.uploadContent(result.result.branchId, result.result.id);

            for (var i = 0; i < this.AllPillarsWithCheckBoxes.length; i++) {
              if (this.AllPillarsWithCheckBoxes[i].checked == true)
                this.AllPillarsWithCheckBoxes[i].checked = false;
            }

            for (var i = 0; i < this.AllSubjectsWithCheckBoxes.length; i++) {
              if (this.AllSubjectsWithCheckBoxes[i].checked == true)
                this.AllSubjectsWithCheckBoxes[i].checked = false;
            }

            for (var i = 0; i < this.AllGradesWithCheckBoxes.length; i++) {
              if (this.AllGradesWithCheckBoxes[i].checked == true)
                this.AllGradesWithCheckBoxes[i].checked = false;
            }

            this.loading = false;
            this.added = true;
            this.edited = false;
            this.contentText = "";
          } else {
            this.error = "Username or password is incorrect";
            this.loading = false;
          }
        },
        error => {
          if (error._body) {
            let body = JSON.parse(error._body);
            let details = body.error.details;
            this.error = body.error.message;
          }
        }
      );
    } else {
      this.modeltopost.id = this.id;
      this.ContentService.UpdateContent(this.modeltopost).subscribe(
        result => {
          if (result.success === true) {
            this.successMessage += "Content updated successfully.";
            obj = "";
            this.loading = false;
            this.added = true;
            this.edited = false;
            this.router.navigateByUrl("/dashboard/ContentManage");
          } else {
            obj = "";
            this.error = "Failed to update Content";
            this.loading = false;
          }
        },
        err => {
          this.error = "Failed to update Content";
          this.contentText = "";
        }
      );
    }
  }

  uploadContent(branchId, contentId) {
    //File Create Attachment and Upload ------
    let fileName = "changepassword"; 
    let fileExtension = "png"; 

    let fileData = {
      branchId: branchId,
      entityId: contentId,
      fileName: fileName,
      extension: fileExtension
    };
    // Entity Type is 8 for Content Attachment
    if (this.fileUploadResponse) {
      let ext = this.fileUploadResponse.fileName.split(".")[1];
      let fileUpdateData = {
        branchId: branchId,
        entityType: 8,
        entityId: contentId,
        fileName: this.fileUploadResponse.fileName,
        extension: ext,
        path: this.fileUploadResponse.path,
        id: this.fileUploadResponse.id
      };
      this.fileService.UpdateAttachment(fileUpdateData).subscribe(
        response => {
          if (response.success == true) {
            this.successMessage += " Attachment updated successfully.";
          }
        },
        error => {
          this.error = "Update attachment request is failed.";
        }
      );
    }
  }

  onAddCard() {
    this.tabsComponent.openTab(
      'New Card',
      this.editFlashCardTemplate, 
      {},
      true
    );
  }

  onEditCard(card: any) {
    this.tabsComponent.openTab(
      `Editing ${card.title}`, 
      this.editFlashCardTemplate, 
      card,
      true
    );
  }

  onDeleteCard(card: any) {
    this.confirmationDialogsService
    .confirm(
      "Confirmation",
      "Do you really want to delete the card?",
      this.viewContainer,
      "  OK  ",
      "Cancel"
    ).subscribe(result => {
      if (result) {
        let index = this.cards.findIndex(c=> c.id = card.id);
        if (index != -1) this.cards.splice(index, 1);
      }
    }); 
  }

  onCardFormSubmit(dataModel) {
    if(dataModel.id > 0) {
      this.cards = this.cards.map(card => {
        if(card.id === dataModel.id) {
          return dataModel;
        } else {
          return card;
        }
      });  
    } else {
      // create a new one
      dataModel.id = Math.round(Math.random()*100);
      this.cards.push(dataModel);
    }
  }
}
