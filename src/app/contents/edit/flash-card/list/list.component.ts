﻿import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, HostListener, Inject, EventEmitter, ChangeDetectorRef, SimpleChanges, Input, Output, AfterViewInit, ViewContainerRef } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FileAttachmentService } from '../../../../_services';
import { ConfirmationDialogsService } from '../../../../ui/confirmation-dialog/_index';


@Component({
	selector: 'app-contents-edit-flash-card-list',
	templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppContentsEditFlashCardListComponent implements OnInit {

	@Input() cards;
	@Output() addCard = new EventEmitter<any>();
	@Output() editCard = new EventEmitter<any>();
	@Output() deleteCard = new EventEmitter<any>();

	constructor(private fileService: FileAttachmentService,
	    		private confirmationDialogsService: ConfirmationDialogsService,
				private viewContainer: ViewContainerRef) { 

	}

	ngOnInit() {

	}

}
