﻿import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, HostListener, Inject, EventEmitter, ChangeDetectorRef, SimpleChanges, Input, Output, AfterViewInit, ViewContainerRef, SecurityContext } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FileAttachmentService } from '../../../../_services';
import { ConfirmationDialogsService } from '../../../../ui/confirmation-dialog/_index';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
	selector: 'app-contents-edit-flash-card-edit',
	templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppContentsEditFlashCardEditComponent implements OnInit {

  cardForm: FormGroup;
  @Input() card;
  @Output() saveCard = new EventEmitter<any>();

	successMessage = "";
	error = "";
	private fileUploadResponse: any;

	constructor(private fileService: FileAttachmentService,
	    				private confirmationDialogsService: ConfirmationDialogsService,
							private viewContainer: ViewContainerRef,
							private sanitizer: DomSanitizer,
							private fb: FormBuilder) { 

		this.cardForm = this.fb.group({
			id: '',
			text: '',
			img: '',
			imgId: '',
			imgName: ''
		});

	}

	ngOnInit() {
    this.cardForm.setValue({
      id: this.card.id || -1,
      text: this.card.text || '',
			img: this.card.img || null,
			imgId: this.card.imgId || -1,
			imgName: this.card.imgName || ''
    });
	}

	onCardFormSubmit() {

		let dataModel = this.cardForm.value;
		dataModel.img = this.card.img;
		dataModel.imgId = this.card.imgId;
		dataModel.imgName = this.card.imgName;

    dataModel.text = this.sanitizer.sanitize(
      SecurityContext.HTML,
      this.card.text
		);
    this.saveCard.emit(dataModel);
  }

	onContentFileSelected(event) {
		let fileList: FileList = event.target.files;
		if (fileList.length > 0) {
		  let file: File = fileList[0];
		  let formData: FormData = new FormData();
		  formData.append("file", file);
		  formData.append("category", "1");
		  formData.append("entityId", "");
	
		  this.fileService.UploadFile(formData).subscribe(
			response => {
			  this.successMessage += "File has been uploaded";
			  if (response.success == true) {
				this.fileUploadResponse = response.result;
				this.card.img = this.fileUploadResponse.path;
				this.card.imgId = this.fileUploadResponse.id;
				this.card.imgName = this.fileUploadResponse.fileName;
			  }
			},
			error => {
			  this.error = "Upload attachment request is failed.";
			}
		  );
		}
	}

	deleteFileAttachment() {
		this.confirmationDialogsService
		.confirm(
		  "Confirmation",
		  "Do you really want to delete the attachment?",
		  this.viewContainer,
		  "  OK  ",
		  "Cancel"
		)
		.subscribe(result => {
		  if (result) {
				this.fileService.Delete(this.card.imgId).subscribe(res => {
					this.card.img = null;
				});
		  }
		});
	}
}
