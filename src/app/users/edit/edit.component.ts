﻿import { error } from 'util';
import { CreateNewAutocompleteGroup } from 'ng-auto-complete';
import { GeneralList } from '../../_models/general-code';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserServices } from '../../_services/index';
import { UserPost } from '../../_models/user'
import { BranchService } from '../../_services/index';
import { BranchResponse } from '../../_models/BranchModel';
import { BranchResult } from '../../_models/BranchModel';
import { Branch } from '../../_models/BranchModel';
import { UserRoleService } from '../../_services/user-role.service';
import { AccountService } from '../../_services/account.service';
import { FileAttachmentService } from '../../_services/file-attachment.service';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
    moduleId: module.id,
    selector: 'app-users-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})

export class AppUsersEditComponent implements OnInit {

    submitted = false;
    model: any = {};
    selectedBranchId: string;
    modeltopost: UserPost;
    BranchesFullResponse: BranchResponse;
    AllBranchesWithCount: BranchResult;
    AllBranchesOnly: Branch[];
    public edited = false;
    public added = false;
    loading = false;
    error = '';
    isError: boolean;
    fullImagePath: string;
    roleList: GeneralList[] = [];
    isEdit: boolean;
    id: any;
    branchId: string;
    username: string;
    firstname: string;
    lastname: string;
    email: string;
    gender:number;
    birthdate:Date;
    userRole: string[] = [];
    SelectedRoles: GeneralList[] = [];
    password: string;
    repassword: string;
    flag: boolean;
    profilePicture='';
    newProfilePicture='';
    fileUploadResponse:any;
    successMessage='';
    profilePicPath = '';
    profilePicInfo: any;
    
    constructor(
        private spinner: NgxSpinnerService,
        private router: Router,
        private userService: UserServices,
        private BranchService: BranchService,
        private BrancesResponse: BranchResponse,
        private userRoles: UserRoleService,
        private param: ActivatedRoute,
        private fileService: FileAttachmentService) {
        this.isEdit = false;
        this.flag = false;
        this.isError = false;
    }


    ngOnInit() {
        this.fullImagePath = '/assets/images/characterly-logo.png'
        this.edited = true;
        this.BranchService.GetBranchesList().subscribe((data) => {
            this.edited = false;
            this.BranchesFullResponse = data;
            this.AllBranchesWithCount = data.result;
            this.AllBranchesOnly = data.result.items;

        }, (err) => {
            console.log(err);
        });

        this.userRoles.GetRolesList()
            .subscribe(response => {
                let result = response.result["items"];
        
                for (let index = 0; index < result.length; index++) {
                    const element = result[index];
                    let role = new GeneralList();
                    role.id = element.name;
                    role.Name = element.displayName;
                    this.roleList.push(role);
                }
            });


        this.param.paramMap
            .subscribe(params => {
                this.id = params.get('id');
                if (this.id != null) {
                    this.isEdit = true;
                }
                else {
                    this.isEdit = false;
                }
            });



    }

    onEdit() {
        if (this.flag == false && this.id != null) {
            this.userService.GetUserById(this.id)
                .subscribe(response => {
                    this.branchId=response.result.branchId ;
                    this.username = response.result.userName;
                    this.firstname = response.result.name;
                    this.lastname = response.result.surname;
                    this.gender = response.result.gender;
                    this.email = response.result.emailAddress;                    
                    this.userRole = response.result.roleNames;
                    this.profilePicture = response.result.profilePicture;
                    this.birthdate =  new Date(response.result.birthdate);
                    
                    this.fileService.GetByEntityId(response.result.id).subscribe( res =>{
                        this.profilePicInfo = res.result[0];
                        if(res.result.length>0){
                            this.profilePicture = res.result[0].id;
                            this.profilePicPath = res.result[0].path;
                        }

                    });

                    //Get User Profile Picture Path
                    // if(this.profilePicture && this.profilePicture){
                    //     this.fileService.GetByEntityId(this.profilePicture ).subscribe( res =>{
                    //         this.profilePicture = res.result.path;
                    //     });
                    // }

                }, (err) => {
                    console.log(err);
                });
            this.flag = true;
        }

    }
    CreateUser(form, f) {
        if(!form.valid){           
            this.submitted=true;
            return;
        }

        if (this.isEdit == false && f.upassword == f.urepassword) {
            console.log("sdfsdf");
            this.loading = true;
            this.modeltopost = new UserPost();
            this.modeltopost.BranchId = this.branchId;
            this.modeltopost.UserName = this.username;
            this.modeltopost.Name = this.firstname;
            this.modeltopost.Surname = this.lastname;
            this.modeltopost.gender = this.gender;
            this.modeltopost.EmailAddress = this.email;
            this.modeltopost.IsActive = true;
            this.modeltopost.roleNames = this.userRole;
            this.modeltopost.profilePicture = this.profilePicture;
            this.modeltopost.Password = this.password;
            this.modeltopost.birthdate = this.birthdate;
     
            this.userService.CreateUser(this.modeltopost)
                .subscribe(result => {
                    if (result.success === true) {
          
                        
                        this.error = '';
                        this.added = true;
                        this.loading = false;

                        if(this.fileUploadResponse && this.newProfilePicture && this.newProfilePicture!=''){
                            this.fileUploadResponse.entityId = result.result.id;
                            if(this.profilePicture){
                                this.fileService.Delete(this.profilePicture).subscribe(
                                    ()=>{this.profilePicture=""},
                                    err=>{});
                            }
                            
                            this.fileService.UpdateAttachment(this.fileUploadResponse).subscribe( 
                                res=>{
                                    this.formReset(form);
                                    this.showSuccessMessage("User has been created successfully.");
                                    this.submitted = false;
                                },err =>{
                                    this.error = 'File Attachment Request is failed';
                                    this.submitted = false;
                                    console.log(err);
                                });
                        }else{
                            this.formReset(form);
                            this.showSuccessMessage("User has been created successfully.");
                            this.submitted = false;
                        }

                    } else {
                        this.error = 'Username or password is incorrect';
                        this.loading = false;
                        this.submitted = false;
                    }
                }, (err) => {
                    this.submitted = false;
                    let errorBody = JSON.parse(err._body);
                    this.error = errorBody.error.message;
                    this.loading = false;
                    console.log(err);
                });
        }
        else if (this.isEdit == true) {
            this.loading = true;
            this.modeltopost = new UserPost();
            this.modeltopost.BranchId = this.branchId;
            this.modeltopost.UserName = this.username;
            this.modeltopost.Name = this.firstname;
            this.modeltopost.Surname = this.lastname;
            this.modeltopost.gender = this.gender;
            this.modeltopost.EmailAddress = this.email;
            this.modeltopost.IsActive = true;
            this.modeltopost.roleNames = this.userRole;
            this.modeltopost.Password = "";
            this.modeltopost.profilePicture = this.profilePicture;
            this.modeltopost.id = this.id;
            this.modeltopost.birthdate = this.birthdate;
            
            this.userService.UpdateUser(this.modeltopost)
                .subscribe(result => {
                    if (result.success === true) {
                        
                            this.error = '';
                            this.loading = false;
                            this.submitted = false;
                            if(this.fileUploadResponse && this.newProfilePicture && this.newProfilePicture !=''){
                                if(this.profilePicture){
                                    this.fileService.Delete(this.profilePicture).subscribe(()=>{this.profilePicture=""});
                                }
                                this.fileUploadResponse.entityId = this.id;
                                this.fileService.UpdateAttachment(this.fileUploadResponse).subscribe( res=>{
                                    this.router.navigateByUrl("/dashboard/UserManage");
                                },err =>{
                                    this.error = 'File Attachment Request is failed';
                                    console.log(err);
                                });
                            }else{
                                this.router.navigateByUrl("/dashboard/UserManage");
                            }

                    } else {
                        this.error = 'Username is incorrect';
                        this.loading = false;
                    }
                }, (err) => {
                  
                    let errorBody = JSON.parse(err._body);
                    this.error = errorBody.error.message;
                    this.loading = false;
                    console.log(err);
                });
        }
        // }
        else {
            this.isError = true;
            this.error = "Password and Re-enter Password is incorrect.";
        }

    }

    showSuccessMessage(message){
        this.successMessage = message;

         setTimeout(() => {
             this.successMessage = '';
         }, 3000);
    }

    formReset(f){
        f.reset();
        this.error='';
        this.newProfilePicture ='';
        //this.successMessage='';
    }
    onImageFileSelected(event){
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            let file: File = fileList[0];           
            let formData:FormData = new FormData();
            formData.append('file', file);
            formData.append('category',  '1');
            formData.append('entityId',  '');
            this.fileService.UploadFile(formData).subscribe(response =>{
                
                // this.successMessage+=" Filed has been uploaded";
                if(response.success == true){
                    this.fileUploadResponse = response.result;
                    this.newProfilePicture = response.result.id;
                    this.profilePicPath = response.result.path;
                    console.log(this.profilePicInfo);
                    if (this.isEdit) {
                        let EntityId = this.profilePicInfo ? this.profilePicInfo.id : null;
                        this.profilePicInfo = response.result;
                        this.profilePicInfo.id = EntityId;
                        console.log(this.profilePicInfo);
                        // this.fileService.UpdateAttachment(this.profilePicInfo).subscribe(response=>{
                        //     console.log(response);
                        // },
                        // error=>{
                        //     this.error = "Upload attachment request is failed."
                        // });
                    }

                }


            },
            error => {
                this.error = "Upload attachment request is failed."
            });
        }

    }

    isRoleSelected(currentRole){
        if(!this.userRole)
            return false;
        if(this.userRole.find( item => item == currentRole.id.toUpperCase())){
                return true;
        }
        return false;
        
    }


}
