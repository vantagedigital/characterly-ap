import 'rxjs/add/operator/switchMap';
import { Component, OnInit, HostBinding } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


import {User} from '../_models/user';
import {UserServices} from '../_services/user.services'


@Component({
  template: `
  <h2>User</h2>
  <div *ngIf="hero$ | async as hero">
    <h3>"{{ hero.name }}"</h3>
    <div>
      <label>ID:  </label>{{ hero.id }}
      <label>Name: </label>{{ hero.name }}
      <label>Email: </label>{{hero.emailAddress}}
      <label>Is Active?: </label>{{hero.isActive}}  
    </div>
    
    <p>
      <button (click)="gotoHeroes(hero)">Back</button>
    </p>
  </div>
  `,

})
export class UserDetailComponent implements OnInit {
//   @HostBinding('@routeAnimation') routeAnimation = true;
//   @HostBinding('style.display')   display = 'block';
//   @HostBinding('style.position')  position = 'absolute';

  hero$: Observable<User>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userservice: UserServices
  ) {}

  ngOnInit() {
    debugger;
    this.hero$ = this.route.paramMap
      .switchMap((params: ParamMap) =>
        this.userservice.getUser(params.get('id')));
  }

  gotoHeroes(hero: User) {
    let heroId = hero ? hero.id : null;
    // Pass along the hero id if available
    // so that the HeroList component can select that hero.
    // Include a junk 'foo' property for fun.
    this.router.navigate(['/ManageUser']);
  }
}


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/