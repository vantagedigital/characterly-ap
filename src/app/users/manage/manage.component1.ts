import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { UserServices } from '../../_services/index';
import { UserResponse } from '../../_models/user';
import { UserResult } from '../../_models/user';
import { User } from '../../_models/user';
import { LoaderService } from '../../ui/loading-spinner/loader.service';
import { LoaderComponent } from '../../ui/loading-spinner/loader.component';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
const DEFAULT_PAGE_SIZE = 5;

@Component({
  moduleId: module.id,
  selector: 'app-users-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css'],
})

export class AppUsersManageComponent implements OnInit, OnDestroy {

  successMessage = "";
  UsersFullResponse: UserResponse;
  AllUsersWithCount: UserResult;
  users: User[]=[];
  selectedUsers: User[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
	filter:string;
	sort:string;
	dir: string;

  loading = false;
  error = '';

  loadingIndicator = false;
  reorderable = true;

  searchControl = new FormControl();
  componentDestroyed = new Subject(); // Component Destroy


  constructor(
    private router: Router,
    private UserService: UserServices,
    private userresponse: UserResponse,
    private loaderService: LoaderService,
    private confirmationDialogsService: ConfirmationDialogsService,
    private viewContainer: ViewContainerRef) { }


  ngOnInit() {
    this.getUsers();

    this.searchControl.valueChanges
		.debounceTime(300) 
      .distinctUntilChanged()  
      .takeUntil(this.componentDestroyed)
      .subscribe( filter => { 
        this.filter = filter ? filter : '';
        this.getUsers();
      });
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }


  getUsers(params=null) {
    this.loadingIndicator = true;
    let query = {
      skipCount: this.pageNumber * this.pageSize,
      maxResultCount: this.pageSize,
      filter: this.filter,
      ...params
    }

    this.UserService.getSearchUsers(query).subscribe(
      (data) => {
        this.loadingIndicator = false;
        this.UsersFullResponse = data;
        this.users = data.result.items;
        this.total = data.result.totalCount;
        console.log(this.total);
      }, (err) => {
        this.loadingIndicator = false;
        console.log(err);
      });    
  }

  deleteUser(Id) {
    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected user?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {
        if(result){
        this.UserService.DeleteUser(Id)
          .subscribe(responce => {
            this.getUsers();
          });
        }
      });
  }


  deleteSelectedUsers() {
    if (this.selectedUsers.length > 0) {

      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected users?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {

          if (result) {
            for (let index = 0; index < this.selectedUsers.length; index++) {
              let id = +this.selectedUsers[index];
              this.UserService.DeleteUser(id)
                .subscribe(responce => {
                  this.selectedUsers.splice(index, 1);
                  this.getUsers();
                }, (err) => {
                  console.log(err);
                });

            }
          }
        });
    } else {
      this.showMessage('error', "Please select the users to delete");

    }
  }

	onSelect({ selected }) {
    this.selectedUsers.splice(0, this.selectedUsers.length);
    this.selectedUsers.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
    this.getUsers();
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getUsers();
  }

  updateFilter(term: string): void {
    this.filter = term;
    this.getUsers();
  }

  min(x, y) {
    return Math.min(x, y);
  }


  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }
}
