import { Component, OnInit, ViewContainerRef, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { UserServices } from '../../_services/index';
import { UserResponse } from '../../_models/user';
import { UserResult } from '../../_models/user';
import { User } from '../../_models/user';
import { LoaderComponent } from '../../ui/loading-spinner/loader.component';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgxSpinnerService } from 'ngx-spinner';

const DEFAULT_PAGE_SIZE = 5;
const MAX_COUNT = 1000;

@Component({
  moduleId: module.id,
  selector: 'app-users-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css'],
})

export class AppUsersManageComponent implements OnInit, OnDestroy {

  successMessage = "";
  error = '';


  users: User[]=[];
  filteredUsers: User[]=[];
  selectedUsers: User[]=[];

  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
  loadingIndicator = false;
  reorderable = true;

  componentDestroyed = new Subject(); // Component Destroy

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput') search: ElementRef;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private UserService: UserServices,
    private userresponse: UserResponse,
    private confirmationDialogsService: ConfirmationDialogsService,
    private viewContainer: ViewContainerRef) { }


  ngOnInit() {
    this.getUsers();
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }


  getUsers() {
    this.spinner.show();
    this.UserService.getUsers(0, MAX_COUNT).subscribe(
      (data) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
        this.users = data.result.items;
        this.total = data.result.totalCount;
        this.updateFilter(this.search.nativeElement.value); 
      }, (err) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
      });    
  }

  deleteUser(userId) {
    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected user?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {
        if(result){
        this.UserService.DeleteUser(userId)
          .subscribe(response => {
            let index = this.users.findIndex(x=>x.id==userId);
            if (index != -1) {
              this.users.splice(index, 1);
              this.updateFilter(this.search.nativeElement.value);
            }
            //this.getUsers();
          });
        }
      });
  }


  deleteSelectedUsers() {
    if (this.selectedUsers.length > 0) {

      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected users?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {

          if (result) {
            for (let index = 0; index < this.selectedUsers.length; index++) {
              let id = +this.selectedUsers[index];
              this.UserService.DeleteUser(id)
                .subscribe(response => {
                  let index = this.users.findIndex(x=>x.id==id);
                  if (index != -1) {
                    this.users.splice(index, 1);
                    this.updateFilter(this.search.nativeElement.value);
                  }
                  //this.getUsers();
                }, (err) => {
                  console.log(err);
                });

            }
          }
        });
    } else {
      this.showMessage('error', "Please select the users to delete");

    }
  }

	onSelect({ selected }) {
    this.selectedUsers.splice(0, this.selectedUsers.length);
    this.selectedUsers.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getUsers();
  }

  updateFilter(term: string): void {
    const val = term.toLowerCase();

    const filteredUsers = this.users.filter(function (cat) {
        return Object.keys(cat).some(key => {
            return (cat[key] ? cat[key] : '')
                .toString()
                .toLowerCase()
                .indexOf(val) !== -1 || !val;
        })
    });

    this.filteredUsers = filteredUsers;
    if (this.table) {
        this.table.offset = 0;
    }
  }

  min(x, y) {
    return Math.min(x, y);
  }


  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }
}
