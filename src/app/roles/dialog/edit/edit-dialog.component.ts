import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, HostListener, Inject, EventEmitter, ChangeDetectorRef, SimpleChanges, Input, Output, AfterViewInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';


export enum KEY_CODE {
    RIGHT_ARROW = 39,
    LEFT_ARROW = 37
}

@Component({
	selector: 'app-roles-edit-dialog',
	templateUrl: './edit-dialog.component.html',
    styleUrls: ['./edit-dialog.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppRolesEditDialogComponent implements OnInit {
	
	role: any;
	permissions: any[]=[];
	formValid: boolean = true;

    constructor(
        public dialogRef: MatDialogRef<AppRolesEditDialogComponent>,		
        @Inject(MAT_DIALOG_DATA) private data: any) { 
			this.role = {...data.role };
			this.permissions = Object.create(data.permissions);
			this.initializeInformation();
	}

	ngOnInit() {

	}


	initializeInformation() {
		this.permissions.forEach(permission =>{
			permission.checked = false;
			let index = this.role.permissions.findIndex(p => p == permission.name );
			if (index != -1) permission.checked = true;
		});
	}

	EditRole() {
        if(!this.role.name || this.role.name.trim() == ''){
            this.formValid=false;
            return;
		}
		this.formValid=true;
		this.role.permissions = [];
        this.permissions.forEach(permission => {
            if (permission.checked) this.role.permissions.push(permission.name);
		});
		
        this.dialogRef.close({ role: this.role });
	}

	updateNewRoleName(roleName: string) {
		if (roleName.trim() != '') this.formValid = true;
	}

	updateAllPemissions(event) {
		this.permissions.forEach(permission =>{
			permission.checked = event.target.checked;
		});
	}

	updateCheckedPemission(permission, event) {
        permission.checked = event.target.checked;
	}


}
