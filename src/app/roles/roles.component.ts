import { RolesService } from './../_services/roles.service';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { GeneralList } from '../_models/general-code';
import { UserRoleService } from '../_services/user-role.service';
import { ResponseContentType } from '@angular/http/src/enums';
import { error, debuglog } from 'util';
import { RoleModel } from '../_models/roleModel';
import { ConfirmationDialogsService } from '../ui/confirmation-dialog/_index';
import { MatDialog } from '@angular/material';
import { AppRolesEditDialogComponent } from './dialog/edit/edit-dialog.component';
import { NgxSpinnerService } from 'ngx-spinner';

const DEFAULT_PAGE_SIZE = 5;
@Component ({
   selector: 'app-roles',
   templateUrl: './roles.component.html',
   styleUrls: ['./roles.component.css'],
})
export class AppRolesComponent implements OnInit{
    successMessage:string='';

    permissions: any=[];
    selectedPermissions: any=[];
    roles:any=[];
    selectedRoles: any[]=[];

    pageSize = DEFAULT_PAGE_SIZE;
    pageLengths=[5, 10, 25, 50];
    total: number = 50;
    pageNumber: number = 0;
    filter:string;
    sort:string;
    dir: string;
  
    loadingIndicator = false;
    reorderable = true;

    newRoleName: string='';
    formValid = true;
    error: string = '';

	dialogRef: any;

    constructor(
        private spinner: NgxSpinnerService,
        private rolesService:RolesService,
        private userRolesService: UserRoleService,
        private confirmationDialogsService: ConfirmationDialogsService,
        private viewContainer   : ViewContainerRef,
		private dialog			: MatDialog
        
    ){

    }

    ngOnInit() {
        this.getAllPermissions();
        this.getRoles();
    }

    getAllPermissions() {
        this.rolesService.GetAllPermissions().subscribe(
            response =>{
                this.permissions = response.result["items"];
            }, 
            err=>{

            });
    }

    getRoles(params=null) {
        let query = {
            skipCount: this.pageNumber * this.pageSize,
            maxResultCount: this.pageSize,
            ...params
        }
        //this.loadingIndicator = true;
        this.spinner.show();
        this.userRolesService.GetRolesList(query).subscribe(
            response => {
                this.spinner.hide();
                //this.loadingIndicator = false;
                this.roles = response.result["items"];
                this.total = response.result.totalCount;
            }, 
            err =>{
                this.spinner.hide();
                //this.loadingIndicator = false;
            });
    }

    editRole(role:any) {
        let newPermissions = JSON.parse(JSON.stringify(this.permissions));
		this.dialogRef = this.dialog.open(AppRolesEditDialogComponent, {
			panelClass: 'roles-edit-dialog',
			data: {
                role: { ...role},
                permissions: newPermissions
			}
		});

		this.dialogRef.afterClosed().subscribe(result => {
			if (result){
                let updateRole = result.role;
                let data=new RoleModel();
                console.log(updateRole);
                data.id = updateRole.id;
                data.Name=updateRole.name;
                data.DisplayName=updateRole.displayName;
                data.Description=updateRole.description;
                data.IsStatic=updateRole.isStatic;
                data.Permissions=updateRole.permissions;
                console.log(data);
                this.updateRole(data);
			}
		});	
    }
    updateRole(data:any) {
        this.rolesService.UpdateRole(data)
        .subscribe(response=>{
            this.showMessage("success","User Role has been updated successfully");
            this.getRoles();
        },(err)=>{
            let errorBody = JSON.parse(err._body);
            this.showMessage("error",errorBody.error.message);
        });
    }

    deleteRole(id) {
        console.log(id);
        this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected user?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {
            if(result){
                this.rolesService.DeleteRole(id).subscribe(
                    response=>{
                        this.getRoles();
                    }, error=>{
                        let errorBody = JSON.parse(error._body);
                        this.showMessage("error",errorBody.error.message);
                    });
            }
        });
    }

    createRole(){
        if(!this.newRoleName || this.newRoleName.trim() == ''){
            this.formValid=false;
            return;
        }
        this.formValid=true;
        let data=new RoleModel();
        data.Name=this.newRoleName;
        data.DisplayName=this.newRoleName;
        data.Description="";
        data.IsStatic=false;
        

        data.Permissions =[];
        this.permissions.forEach(permission => {
            if (permission.checked) data.Permissions.push(permission.name);
        });
        console.log(data);
        this.rolesService.CreateRole(data)
        .subscribe(response=>{
            this.showMessage("success","User Role has been added successfully");
            this.permissions.forEach(permission => permission.checked = false);
            this.newRoleName = '';
            this.getRoles();
        },(err)=>{
            let errorBody = JSON.parse(err._body);
            this.showMessage("error",errorBody.error.message);
        });
    }

    onSort(event) {
        // this.sort = event.sorts[0].prop;
        // this.dir = event.sorts[0].dir;
        // this.getContents();
      }
    
      setPage(pageInfo) {
        this.pageNumber = pageInfo.page - 1;
        this.getRoles();
      }
    
      onPageLengthChange(value) {
        this.pageSize = value;
        this.getRoles();
      }
    onSelect(evt) {
    }

    onActivate(evt) {     
    } 

    updateAllPemissions(event) {
		this.permissions.forEach(permission =>{
			permission.checked = event.target.checked;
		});
    }

    updateCheckedPemission(permission, event) {
        permission.checked = event.target.checked;
    }

    updateNewRoleName(roleName: string) {
        if (roleName.trim() != '') this.formValid = true;
    }

    showMessage(type, message) {
        if (type == 'success')
          this.successMessage = message;
        else
          this.error = message;
    
        setTimeout(() => {
          this.successMessage = '';
          this.error = '';
        }, 3000);
    }

    min(x, y) {
        return Math.min(x, y);
    }
}