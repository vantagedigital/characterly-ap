import { Component, OnInit, ViewContainerRef, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { BranchService } from '../../_services/index';
import { BranchResponse } from '../../_models/BranchModel';
import { BranchResult } from '../../_models/BranchModel';
import { Branch } from '../../_models/BranchModel';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgxSpinnerService } from 'ngx-spinner';

const DEFAULT_PAGE_SIZE = 5;
const MAX_COUNT = 1000;

@Component({
  selector: 'app-branches-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class AppBranchesManageComponent implements OnInit, OnDestroy {

  successMessage = '';
  error = '';

  branches: Branch[]=[];
  filteredBranches: Branch[]=[];
  selectedBranches: Branch[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
  loadingIndicator = false;
  reorderable = true;

  componentDestroyed = new Subject(); // Component Destroy
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('searchInput') search: ElementRef;

  constructor(
    private spinner: NgxSpinnerService,
    private router: Router,
    private viewContainer: ViewContainerRef,
    private BranchService: BranchService,
    private BrancesResponse: BranchResponse,
    private confirmationDialogsService: ConfirmationDialogsService) {
  }




  ngOnInit() {
    this.getBranches();
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  getBranches() {
    this.spinner.show();
    this.BranchService.getBranches(0, MAX_COUNT).subscribe(
      (data) => {
        this.spinner.hide();
        //this.loadingIndicator = false;
        this.branches = data.result.items;
        this.total = data.result.totalCount;
        this.updateFilter(this.search.nativeElement.value); 
      }, (err) => {
        this.spinner.hide();
        //this.loadingIndicator = false;

      });    
  }

  deleteBranch(branchId) {
    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected branch?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {

        if (result) {
          this.BranchService.DeleteBranch(branchId)
            .subscribe(response => {
              let index = this.branches.findIndex(x=>x.id==branchId);
              if (index != -1) {
                this.branches.splice(index, 1);
                this.updateFilter(this.search.nativeElement.value);
              }
              //this.getBranches();
            }, (err) => {
              console.log(err);
            });
        }

      });

  }

  deleteSelectedBranches() {

    if (this.selectedBranches.length > 0) {

      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected branches?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {
          if (result) {
            for (let index = 0; index < this.selectedBranches.length; index++) {
              let id = this.selectedBranches[index].id;
              this.BranchService.DeleteBranch(id).subscribe(
                response => {
                  let index = this.branches.findIndex(x=>x.id==id);
                  if (index != -1) {
                    this.branches.splice(index, 1);
                    this.updateFilter(this.search.nativeElement.value);
                  }
                  //this.getBranches();
                }, (err) => {
                  console.log(err);
                });

            }
          }
        });
    } else {
      this.showMessage('error', "Please select the branch to delete");
    }

  }

	onSelect({ selected }) {
    this.selectedBranches.splice(0, this.selectedBranches.length);
    this.selectedBranches.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
    //this.getBranches();
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getBranches();
  }

  updateFilter(term: string): void {
    const val = term.toLowerCase();

    const filteredBranches = this.branches.filter(function (cat) {
        return Object.keys(cat).some(key => {
            return (cat[key] ? cat[key] : '')
                .toString()
                .toLowerCase()
                .indexOf(val) !== -1 || !val;
        })
    });

    this.filteredBranches = filteredBranches;
    if (this.table) {
        this.table.offset = 0;
    }
  }

  min(x, y) {
    return Math.min(x, y);
  }

  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }

}



