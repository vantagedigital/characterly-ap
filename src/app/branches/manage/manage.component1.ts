import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { BranchService } from '../../_services/index';
import { BranchResponse } from '../../_models/BranchModel';
import { BranchResult } from '../../_models/BranchModel';
import { Branch } from '../../_models/BranchModel';
import { ConfirmationDialogsService } from '../../ui/confirmation-dialog/_index';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
const DEFAULT_PAGE_SIZE = 5;

@Component({
  selector: 'app-branches-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class AppBranchesManageComponent implements OnInit, OnDestroy {

  successMessage = '';
  BranchesFullResponse: BranchResponse;
  AllBranchesWithCount: BranchResult;
  AllBranchesOnly: Branch[];
  branches: Branch[]=[];
  selectedBranches: Branch[]=[];
  pageSize = DEFAULT_PAGE_SIZE;
  pageLengths=[5, 10, 25, 50];
  total: number = 50;
  pageNumber: number = 0;
	filter:string;
	sort:string;
	dir: string;

  loading = false;
  error = '';

  loadingIndicator = true;
  reorderable = true;

  searchControl = new FormControl();
  componentDestroyed = new Subject(); // Component Destroy

  constructor(
    private router: Router,
    private viewContainer: ViewContainerRef,
    private BranchService: BranchService,
    private BrancesResponse: BranchResponse,
    private confirmationDialogsService: ConfirmationDialogsService) {
  }




  ngOnInit() {
    this.getBranches();

    this.searchControl.valueChanges
		.debounceTime(300) 
      .distinctUntilChanged()  
      .takeUntil(this.componentDestroyed)
      .subscribe( filter => { 
        this.filter = filter ? filter : '';
        this.getBranches();
      });
  }

  ngOnDestroy() {
    this.componentDestroyed.next();
    this.componentDestroyed.unsubscribe();
  }

  getBranches(params=null) {
    this.loadingIndicator = true;
    let query = {
      skipCount: this.pageNumber * this.pageSize,
      maxResultCount: this.pageSize,
      filter: this.filter,
      ...params
    }

    this.BranchService.getSearchBranches(query).subscribe(
      (data) => {
        this.loadingIndicator = false;
        this.BranchesFullResponse = data;
        this.branches = data.result.items;
        this.total = data.result.totalCount;
        console.log(this.total);
      }, (err) => {
        this.loadingIndicator = false;
        console.log(err);
      });    
  }

  deleteBranch(id) {
    this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected branch?", this.viewContainer, "  OK  ", "Cancel")
      .subscribe(result => {

        if (result) {
          this.BranchService.DeleteBranch(id)
            .subscribe(responce => {
              this.getBranches();
            }, (err) => {
              console.log(err);
            });
        }

      });

  }

  deleteSelectedBranches() {

    if (this.selectedBranches.length > 0) {

      this.confirmationDialogsService.confirm("Confirmation", "Do you really want to delete the selected branches?", this.viewContainer, "  OK  ", "Cancel")
        .subscribe(result => {
          if (result) {
            for (let index = 0; index < this.selectedBranches.length; index++) {
              let id = this.selectedBranches[index];
              this.BranchService.DeleteBranch(id)
                .subscribe(responce => {
                  this.selectedBranches.splice(index, 1);
                  this.getBranches();
                }, (err) => {
                  console.log(err);
                });

            }
          }
        });
    } else {
      this.showMessage('error', "Please select the branch to delete");
    }

  }

	onSelect({ selected }) {
    this.selectedBranches.splice(0, this.selectedBranches.length);
    this.selectedBranches.push(...selected);
  }

  onActivate(evt) {
  }

  onSort(event) {
    // this.sort = event.sorts[0].prop;
    // this.dir = event.sorts[0].dir;
    // this.getContents();
  }

  setPage(pageInfo) {
    this.pageNumber = pageInfo.page - 1;
    this.getBranches();
  }

  onPageLengthChange(value) {
    this.pageSize = value;
    this.getBranches();
  }

  updateFilter(term: string): void {
    this.filter = term;
    this.getBranches();
  }

  min(x, y) {
    return Math.min(x, y);
  }

  showMessage(type, message) {
    if (type == 'success')
      this.successMessage = message;
    else
      this.error = message;

    setTimeout(() => {
      this.successMessage = '';
      this.error = '';
    }, 3000);
  }

}



