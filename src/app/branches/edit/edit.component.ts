﻿import { GeneralList } from '../../_models/general-code';
import { CountryService } from '../../_services/country.service';

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserServices } from '../../_services/index';
import { UserPost } from '../../_models/user'
import { BranchService } from '../../_services/index';
import { BranchResponse } from '../../_models/BranchModel';
import { BranchResult } from '../../_models/BranchModel';
import { Branch } from '../../_models/BranchModel';
import { BranchPost } from '../../_models/BranchModel';
import "../../../assets/js/jquery.tree-multiselect.min.js";
// declare var Taggle: any;
declare var jQuery: any;

@Component({
    moduleId: module.id,
    selector: 'app-branches-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})

export class AppBranchesEditComponent implements OnInit {
    
    submitted=false;
    successMessage ='';
    model: any = {};
    selectedBranchId: string;
    modeltopost: BranchPost;
    BranchesFullResponse: BranchResponse;
    AllBranchesWithCount: BranchResult;
    AllBranchesOnly: Branch[];
    public edited = false;
    public added = false;
    loading = false;
    error = '';
    fullImagePath: string;
    countryList: GeneralList[] = [];
    selectedCountry: any;
    id: any;
    name: string;
    region: string;
    numberOfSchools:number;
    numberOfEducators:number;
    numberOfParents:number;
    numberOfStudents:number;
    primaryContactName: string;
    primaryContactEmail: string;
    primaryContactPhoneNumber: string;
    isEdit:boolean;
    flag:boolean;

    constructor(
        private router: Router,
        private userService: UserServices,
        private BranchService: BranchService,
        private BrancesResponse: BranchResponse,
        private country: CountryService,
        private param: ActivatedRoute) {

        this.isEdit = false;
        this.flag = false;
    }

    ngOnInit() {
        // debugger;
        // setTimeout(() => {
        //     new Taggle('branch-tags');
        //     function treeOnChange(allSelectedItems, addedItems, removedItems, unselectAllText) {
        //         console.log("something changed!");
        //     }
        //     jQuery("#branch-multiselect").treeMultiselect({
        //         allowBatchSelection: true,
        //         onChange: treeOnChange,
        //         startCollapsed: true,
        //         searchable: true,
        //         sortable: false
        //     });
        // }, 0);

        this.fullImagePath = '/assets/images/characterly-logo.png';

        this.param.paramMap
            .subscribe(params => {
                debugger;
                this.id = params.get('id');
                if (this.id != null) {
                    this.isEdit = true;
                }
                else {
                    this.isEdit = false;
                }
            });

        this.country.getAll()
            .subscribe(responce => {
                debugger;
                for (let index = 0; index < responce.result.length; index++) {
                    const element = responce.result[index];
                    let country = new GeneralList();
                    country.id = element.id;
                    country.Name = element.name;
                    this.countryList.push(country);
                }

                if (this.isEdit==true) {
                    this.BranchService.GetBranchById(this.id)
                    .subscribe(responce=>{
                        this.name = responce.result.name;
                        this.region = responce.result.region;
                        this.selectedCountry=responce.result.countryId;
                        this.numberOfSchools = responce.result.numberOfSchools;
                        this.numberOfEducators = responce.result.numberOfEducators;
                        this.numberOfParents = responce.result.numberOfParents;
                        this.numberOfStudents = responce.result.numberOfStudents;
                        this.primaryContactName = responce.result.primaryContactName;
                        this.primaryContactEmail = responce.result.primaryContactEmail;
                        this.primaryContactPhoneNumber = responce.result.primaryContactPhoneNumber;
                    });
                }
            });
    }

    CreateBranch(f) {
        if(!f.valid){
            this.submitted=true;
            return;
        }
      
        if (this.isEdit==false) {
            console.log(this.model);
            this.edited = true;
            this.loading = true;
            this.modeltopost = new BranchPost();
            this.modeltopost.countryId = this.selectedCountry;
            this.modeltopost.stateId = 0;
            this.modeltopost.name = this.name;
            this.modeltopost.region = this.region;
            this.modeltopost.numberOfSchools = this.numberOfSchools;
            this.modeltopost.numberOfEducators = this.numberOfEducators;
            this.modeltopost.numberOfParents = this.numberOfParents;
            this.modeltopost.numberOfStudents=this.numberOfStudents;
            this.modeltopost.primaryContactName = this.primaryContactName;
            this.modeltopost.primaryContactEmail = this.primaryContactEmail;
            this.modeltopost.primaryContactPhoneNumber = this.primaryContactPhoneNumber;
            this.BranchService.CreateBranch(this.modeltopost)
                .subscribe(result => {
                    if (result.success === true) {
                        debugger;
                        this.loading = false;
                        this.added = true;
                        this.edited = false;
                        this.showMessage("success", 'Branch Created Successfully');
                        f.reset();
                        this.submitted=false;
                    } else {
                        this.error = 'Username or password is incorrect';
                        this.loading = false;
                        this.showMessage("error", this.error);
                    }
                },error =>{
                    if(error._body){
                        let errorObj = JSON.parse(error._body);
                        this.error = errorObj.error.details 
                        this.showMessage("error", this.error );
                      }
                });
        }
        else if (this.isEdit==true) {
            console.log(this.model);
            this.edited = true;
            this.loading = true;
            this.modeltopost = new BranchPost();
            this.modeltopost.countryId = this.selectedCountry;
            this.modeltopost.stateId = 0;
            this.modeltopost.name = this.name;
            this.modeltopost.region = this.region;
            this.modeltopost.numberOfSchools = this.numberOfSchools;
            this.modeltopost.numberOfEducators = this.numberOfEducators;
            this.modeltopost.numberOfParents = this.numberOfParents;
            this.modeltopost.numberOfStudents = this.numberOfStudents; 
            this.modeltopost.primaryContactName = this.primaryContactName;
            this.modeltopost.primaryContactEmail = this.primaryContactEmail;
            this.modeltopost.primaryContactPhoneNumber = this.primaryContactPhoneNumber;
            this.modeltopost.id=this.id;
            this.BranchService.UpdateBranch(this.modeltopost)
                .subscribe(result => {
                    if (result.success === true) {
                        debugger;
                        this.loading = false;
                        this.added = true;
                        this.edited = false;
                    } else {
                        this.error = 'Username or password is incorrect';
                        this.showMessage("error", this.error );
                        this.loading = false;
                    }
                    this.router.navigateByUrl("/dashboard/BranchManage");
                },error =>{
                    if(error._body){
                        let errorObj = JSON.parse(error._body);
                        this.error = errorObj.error.details 
                      }
                });
        }
       
    }

    showMessage(type, message){
        if(type='success')
          this.successMessage = message;
        else
          this.error = message;
    
         setTimeout(() => {
             this.successMessage = '';
             this.error = '';
         }, 3000);
    }
}
