import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {FormBuilder,FormControl,FormGroup,Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
 import { AccountService, UserServices } from '../../_services/index';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.css']
})
export class RecoverPasswordComponent implements OnInit {

  model: any = {};
  loading = false;
  message = '';
  error = '';
  email:string;
  token:string;
  recoverPasswordForm:FormGroup;
  private formSubmitAttempt: boolean;

  constructor(
        private router: Router,
        private activatedRoute:ActivatedRoute,
        private accountService: AccountService,
        private snackBar:MatSnackBar,
        private fb:FormBuilder,
        private userServices:UserServices
  ) { }

  ngOnInit() {
     let p = this.activatedRoute.queryParams.subscribe( params =>{

      if(params.email && params.token){
        this.email = params.email;
        this.token = params.token;
        
        this.recoverPasswordForm = this.fb.group(
          {
            password: ['', Validators.required],
            confirmPass: ['', Validators.required]
          },
          {validator: this.checkIfMatchingPasswords('password', 'confirmPass')}
        ); 
        // this.recoverPasswordForm = this.fb.group(
        //   {
        //     passwords: this.fb.group({
        //       password: ['', [Validators.required]],
        //       confirmPass: ['', [Validators.required]]
        //     }, {validator: this.areEqual})
        //   }
        // );
      }

     });

    
 
  }
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
          passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else {
          return passwordConfirmationInput.setErrors(null);
      }
    }
  }
  recoverPassword(){
    if(!this.recoverPasswordForm.valid){
      this.formSubmitAttempt = true;
      return;
    }
    //this.snackBar.open('A link has been sent to your provided email.', "OK" ,{ duration: 5000 } );
    this.loading = true;
    
    if(this.email != "" && this.token != ""){
    
    let password = this.recoverPasswordForm.value.password;
      this.accountService.RecoverPassword(this.email,password,this.token)
      .subscribe(result => {
          if (result && result.success === true) {
            this.message = 'Your code has successfully sent to your Email. ';
            this.loading = false;
          } 
      },
      (error)=>{
        if(error._body){
          this.error = JSON.parse(error._body).error.message
        }
        
        this.loading = false;
      });
    }
    else{
      this.error = "Email/Token is not provided";
    }
    

  }

}
