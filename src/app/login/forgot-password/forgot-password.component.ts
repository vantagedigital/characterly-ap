import { Component, OnInit, } from '@angular/core';
import { Router } from '@angular/router';
import {MatSnackBar} from '@angular/material';
 import { AccountService } from '../../_services/index';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css'],
  providers: [AccountService]
})
export class ForgotPasswordComponent implements OnInit {
  model: any = {};
  loading = false;
  message = '';
  error = '';
  constructor(
        private router: Router,
        private accountService: AccountService,
        private snackBar:MatSnackBar
  ) { }

  ngOnInit() {
  }
  forgetpassword(){
    //this.snackBar.open('A link has been sent to your provided email.', "OK" ,{ duration: 5000 } );
    this.loading = true;


    this.accountService.ChangePassword(this.model.emailAddress)
    .subscribe(result => {
        if (result && result.success === true) {
          this.message = 'Your code has successfully sent to your Email. ';
          this.loading = false;
        } 
    },
    (error)=>{
      if(error._body){
        this.error = JSON.parse(error._body).error.message
      }
      
      this.loading = false;
    });
  }

}
