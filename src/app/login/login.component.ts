﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services/index';
import { CookieService } from 'angular2-cookie/core';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
    fullImagePath: string;
    rememberMe:Boolean;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private cookieService:CookieService) { }

    ngOnInit() {
        // reset login status
        this.fullImagePath = '/assets/images/characterly-logo.png'
        
        

        //localStorage.setItem('userId',response.json().result.userId);
        // store username and jwt token in local storage to keep user logged in between page refreshes
        let rememberMeStatus = localStorage.getItem('rememberMe');
        if(rememberMeStatus == "true"){
            this.rememberMe = true;
        }
        else{
            this.rememberMe = false;
        }
       debugger;
       if(this.rememberMe){
           let user:any = localStorage.getItem('currentUser');
           if(user){
               user = JSON.parse(user);
            this.authenticationService.token = user.token;
            this.model.username = user.username;
            this.model.password=this.cookieService.get(this.model.username);
           }
       }else{
           this.authenticationService.logout();
       }


    }

    login() {
        
        this.loading = true;
        localStorage.setItem("rememberMe", this.rememberMe.toString()); 


        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(result => {
                if (result === true) {
                    //this.router.navigate(["/?refresh=1"]);
                    this.router.navigate(['/dashboard/Overview']);
                    
                    let date = new Date(2036, 12, 31);
                    this.cookieService.put(this.model.username, this.model.password, {'expires': date});
                    //window.location.reload();
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            });
    }
}
